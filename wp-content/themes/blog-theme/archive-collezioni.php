<?php
/*
Template Name: Template Collezioni
*/
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/collezioni/archive/slide'); ?>
    <?php get_template_part('templates/collezioni/archive/title'); ?>
    <?php get_template_part('templates/collezioni/archive/loop'); ?>
    <?php get_template_part('templates/collezioni/download'); ?>
<?php endwhile; ?>
