<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/title'); ?>
  <?php get_template_part('templates/content'); ?>
<?php endwhile; ?>