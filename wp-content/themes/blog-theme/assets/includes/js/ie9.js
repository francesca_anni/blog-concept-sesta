// Popup IE9

$(document).ready(function () {

    $('body').prepend('<div id="ie-compatibility-fail"><div class="content"><i class="fa fa-close closeIe9"></i><p>Stai usando una versione non aggiornata di Internet Explorer!</p><p>Per maggior <b>compatibilità</b>, <b>sicurezza</b> e <b>velocità</b> ti invitiamo ad aggiornarlo o a installare uno dei browser qui sotto!</p><p class="links"></p></div></div>');

    var browsers = [];
        browsers.push({
            img: 'img/icon-firefox.png',
            url: 'https://www.mozilla.org/it/firefox/new/',
            label: 'Mozilla Firefox'
        });
        browsers.push({
            img: 'img/icon-chrome.png',
            url: 'https://www.google.it/chrome/browser/desktop/',
            label: 'Chrome'
        });
        browsers.push({
            img: 'img/icon-safari.png',
            url: 'http://www.apple.com/it/safari/',
            label: 'Safari'
        });
        browsers.push({
            img: 'img/icon-opera.png',
            url: 'http://www.opera.com/it/computer',
            label: 'Opera'
        });
        browsers.push({
            img: 'img/icon-ie.png',
            url: 'http://windows.microsoft.com/it-it/internet-explorer/download-ie',
            label: 'Internet Explorer'
        });
    $.each(browsers, function (k,el) {
        $('#ie-compatibility-fail .links').append($('<a>').attr('href', el.url).attr('target', '_blank').html(
            $('<img>').attr('src', el.img).attr('title', el.label)
        ));
    });
    $('.closeIe9').click(function(){
        $('#ie-compatibility-fail').fadeOut();
    });

});