<?php
/*
Template Name: Template Home
*/
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/home/slide'); ?>
    <?php get_template_part('templates/home/content'); ?>
<?php endwhile; ?>
