<?php
/*
Template Name: Template Login
*/
?>

<?php

$action = !empty( $_GET['action'] ) && ($_GET['action'] == 'register' || $_GET['action'] == 'forgot' || $_GET['action'] == 'resetpass') ? $_GET['action'] : 'login';
$success = !empty( $_GET['success'] );
$failed = !empty( $_GET['failed'] ) ? $_GET['failed'] : false;

?>

<section class="tit-page">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-12 text-center">
                <?php
                $blog_id = get_current_blog_id();
                if ( 1 == $blog_id ) { ?>
                    <h2 class="tit-section tit-second orange">Area Riservata agli operatori di settore</h2>
                <?php } if ( 2 == $blog_id ) { ?>
                    <h2 class="tit-section tit-second orange">RESERVED AREA TO THE SECTOR OPERATORS</h2>
                <?php } if ( 3 == $blog_id ) { ?>
                    <h2 class="tit-section tit-second orange">ZONE RÉSERVÉE POUR LES OPÉRATEURS DU SECTEUR</h2>
                <?php } ?>
            </div>

	        <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-12 col-md-4">
                <div id="tab-login" class="tab-content">
                    <?php
                    $blog_id = get_current_blog_id();
                    if ( 1 == $blog_id ) { ?>
                        <p>Sei già registrato? Accedi alle informazioni.Se avete dimenticato nome utente e password o avete problemi di accesso mandate una e-mail all'indirizzo <a href="mailto:info@sesta.it" target="_blank" title="Invia Email">info@sesta.it</a></p>
                    <?php } if ( 2 == $blog_id ) { ?>
                        <p>Already a member? If you have forgotten your username and password or have access problems send an e-mail to <a href="mailto:info@sesta.it" target="_blank" title="Invia Email">info@sesta.it</a></p>
                    <?php } if ( 3 == $blog_id ) { ?>
                        <p>Déjà membre? Si vous avez oublié votre nom d’utilisateur et mot de passe ou que vous avez des problèmes d’accès envoyer un e-mail à <a href="mailto:info@sesta.it" target="_blank" title="Invia Email">info@sesta.it</a></p>
                    <?php } ?>

                    <?php if ( $action == 'login' && $failed ): ?>
                        <div class="message-box message-error errore-login">
                            <span class="icon-attention"></span>
                            <?php if ( $failed ): ?>
                                <?php
                                $blog_id = get_current_blog_id();
                                if ( 1 == $blog_id ) { ?>
                                    <b>Nome utente o password errati.<br>Per favore riprova.</b>
                                <?php } if ( 2 == $blog_id ) { ?>
                                    <b>Invalid username or password.<br>Please try again.</b>
                                <?php } if ( 3 == $blog_id ) { ?>
                                    <b>Nom d'utilisateur ou mot de passe invalide.<br>Veuillez réessayer.</b>
                                <?php } ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <?php wp_login_form(); ?>

                </div>
            </div>

            <div class="col-12 col-md-6">
                <div id="tab-register" class="tab-content">
                    <?php
                    $blog_id = get_current_blog_id();
                    if ( 1 == $blog_id ) { ?>
                        <p>Per accedere all'area riservata è necessario richiedere la password compilando il form sottostante.</p>
                        <?php echo do_shortcode('[contact-form-7 id="536" title="Nuovo Utente"]'); ?>
                    <?php } if ( 2 == $blog_id ) { ?>
                        <p>To access this area you need to request the password by filling out the form below.</p>
                        <?php echo do_shortcode('[contact-form-7 id="515" title="Nuovo Utente"]'); ?>
                    <?php } if ( 3 == $blog_id ) { ?>
                        <p>Pour accéder à cette zone, vous devez demander le mot de passe en remplissant le formulaire ci-dessous.</p>
                        <?php echo do_shortcode('[contact-form-7 id="501" title="Nuovo Utente"]'); ?>
                    <?php } ?>
                </div>
            </div>

            <div class="col-12">
                <?php
                $blog_id = get_current_blog_id();
                if ( 1 == $blog_id ) { ?>
                    <h3>Se non sei un'operatore di settore<br>
                        <a href="/arredo-ufficio-design/" target="_self" title="Contattaci per avere informazioni">contattaci per avere informazioni</a>.
                    </h3>
                <?php } if ( 2 == $blog_id ) { ?>
                    <h3>If you are not a business operator please<br>
                        <a href="/office-furniture-design/" target="_self" title="Contact us for information">contact us for information</a>.
                    </h3>
                <?php } if ( 3 == $blog_id ) { ?>
                    <h3>Si vous n’êtes pas un opérateur du secteur<br>
                        <a href="/mobilier-bureau-design/" target="_self" title="Contactez-nous pour plus d'informations">contactez nous</a> nous pour avoir des infos.
                    </h3>
                <?php } ?>
            </div>
	        <?php endwhile; ?>

        </div>
    </div>
</section>
