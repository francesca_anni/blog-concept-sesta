<?php
/*-----------------------------------------------------------------------------------*/
/*  Dexanet theme options
/*-----------------------------------------------------------------------------------*/

if (is_admin()) {
    require_once (get_stylesheet_directory() . '/theme-options/theme-options.php');

}

/*-----------------------------------------------------------------------------------*/
/*  MadFramework core
/*-----------------------------------------------------------------------------------*/
/**
 * MadFramework includes
 *
 * The $mad_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 */
$mad_includes = array(
    'plugins-activation/plugins-activation.php', // Plugins
    'core/utils.php', // Utility functions
    'core/init.php', // Initial theme setup and constants
    'core/wrapper.php', // Theme wrapper class
    'core/config.php', // Configuration
    'core/activation.php', // Theme activation
    'core/titles.php', // Page titles
    'core/nav.php', // Custom nav modifications
    'core/scripts.php', // Scripts and stylesheets
    'core/write.php', // Writing something on files
    'core/extras.php', // Custom functions
    'core/area-riservata.php',  // Protected area
);

if (class_exists('Timber')) {
    Timber::$cache = true;
}

foreach($mad_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'mad') , $file) , E_USER_ERROR);
    }

    require_once $filepath;

}

unset($file, $filepath);

function custom_pagination($numpages = '', $pagerange = '', $paged = '')
{
    if (empty($pagerange)) {
        $pagerange = 2;
    }

    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     *
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }

    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if (!$numpages) {
            $numpages = 1;
        }
    }

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function.
     */
    $pagination_args = array(
        'base' => get_pagenum_link(1) . '%_%',
        'format' => 'page/%#%',
        'total' => $numpages,
        'current' => $paged,
        'show_all' => False,
        'end_size' => 1,
        'mid_size' => $pagerange,
        'prev_next' => True,
        'prev_text' => __('&laquo;') ,
        'next_text' => __('&raquo;') ,
        'type' => 'plain',
        'add_args' => false,
        'add_fragment' => ''
    );
    $paginate_links = paginate_links($pagination_args);
    if ($paginate_links) {
        echo "<nav class='custom-pagination'>";
        echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
        echo $paginate_links;
        echo "</nav>";
    }
}

/*** Disable default media organization Media ***/
$uploads_use_yearmonth_folders = get_option('uploads_use_yearmonth_folders', 1);

if ($uploads_use_yearmonth_folders) {
    update_option('uploads_use_yearmonth_folders', 0);
}

/*** Dexanet Dasboard ***/
add_action('wp_dashboard_setup', 'dexanet_dashboard_widgets');

function dexanet_dashboard_widgets()
{
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Dexanet theme', 'custom_dashboard_help');
}

function custom_dashboard_help()
{
    echo '<p>
        Benvenuto in Dexanet Theme.<br /> 
        Sviluppato in <a target="_blank" href="http://getbootstrap.com/">Bootstrap 4.</a><br />
        Framework: <a target="_blank" href="https://timber.github.io/docs/">Timber</a><br />
        Info: <a href="mailto:assistenza@dexanet.com">assistenza@dexanet.com</a>
      </p>
      <img style="width:100%;" src="' . get_template_directory_uri() . '/logo.png" alt="">';
}

// Footer widget

function arphabet_widgets_init()
{
    register_sidebar(array(
        'name' => 'Footer 1',
        'id' => 'footer_bottom',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => 'Footer 2',
        'id' => 'footer_bottom_2',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => 'Footer 3',
        'id' => 'footer_bottom_3',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => 'Footer 4',
        'id' => 'footer_bottom_4',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'arphabet_widgets_init');

// Use this widget code on template page to show widget content
// Sono già inseriti nel footer

/*
<?php if ( is_active_sidebar( 'footer_bottom' ) ) : ?>
<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
<?php dynamic_sidebar( 'footer_bottom' ); ?>
</div><!-- #footer area -->
<?php endif; ?>
*/

function get_breadcrumb_yoast()
{
    yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
}

/**
Connessione server SMTP
 */
$enable_smtp = get_option('enable_smtp');

if ($enable_smtp == 1) {
    add_action('phpmailer_init', 'send_smtp_email');
    function send_smtp_email($phpmailer)
    {
        $SMTP_HOST = get_option('host_smtp');
        $SMTP_AUTH = get_option('auth_smtp');
        $SMTP_PORT = get_option('port_smtp');
        $SMTP_USER = get_option('user_smtp');
        $SMTP_PASS = get_option('pass_smtp');
        $SMTP_SECURE = get_option('secure_smtp');
        $SMTP_FROM = get_option('from_smtp');
        $SMTP_NAME = get_option('name_smtp');
        $phpmailer->isSMTP();
        $phpmailer->Host = "$SMTP_HOST";
        $phpmailer->SMTPAuth = $SMTP_AUTH;
        $phpmailer->Port = "$SMTP_PORT";
        $phpmailer->Username = "$SMTP_USER";
        $phpmailer->Password = "$SMTP_PASS";
        $phpmailer->SMTPSecure = "$SMTP_SECURE";
        $phpmailer->SMTPAutoTLS = false;
        $phpmailer->From = "$SMTP_FROM";
        $phpmailer->FromName = "$SMTP_NAME";
    }
}

/***
Riduzione del numero di immagini generate da wordpress upload media
 ***/

function wpmayor_filter_image_sizes($sizes)
{
    unset($sizes['medium']); // Remove Thumbnail (150 x 150 hard cropped)
    unset($sizes['medium']); // Remove Medium resolution (300 x 300 max height 300px)
    unset($sizes['medium_large']); // Remove Medium Large (added in WP 4.4) resolution (768 x 0 infinite height)
    unset($sizes['large']); // Remove Large resolution (1024 x 1024 max height 1024px)
    /* With WooCommerce */
    unset($sizes['shop_thumbnail']); // Remove Shop thumbnail (180 x 180 hard cropped)
    unset($sizes['shop_catalog']); // Remove Shop catalog (300 x 300 hard cropped)
    unset($sizes['shop_single']); // Shop single (600 x 600 hard cropped)
    return $sizes;
}

add_filter('intermediate_image_sizes_advanced', 'wpmayor_filter_image_sizes');
/***
Disabilitare i commenti ovunque
 ***/

function df_disable_comments_post_types_support()
{
    $post_types = get_post_types();
    foreach($post_types as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
}

add_action('admin_init', 'df_disable_comments_post_types_support');

function df_disable_comments_status()
{
    return false;
}

add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);

function df_disable_comments_hide_existing_comments($comments)
{
    $comments = array();
    return $comments;
}

add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);

function df_disable_comments_admin_menu()
{
    remove_menu_page('edit-comments.php');
}

add_action('admin_menu', 'df_disable_comments_admin_menu');

function df_disable_comments_admin_menu_redirect()
{
    global $pagenow;
    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());
        exit;
    }
}

add_action('admin_init', 'df_disable_comments_admin_menu_redirect');

function df_disable_comments_dashboard()
{
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}

add_action('admin_init', 'df_disable_comments_dashboard');

function df_disable_comments_admin_bar()
{
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
}

add_action('init', 'df_disable_comments_admin_bar');


// SVG NEI MEDIA
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    $mimes['zip'] = 'application/zip';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


// SEO BRICIOLE
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

function wpse_100012_override_yoast_breadcrumb_trail( $links ) {
    global $post;

    if ( is_home() || is_singular( 'collezioni' ) || is_archive() ) {
        $breadcrumb[] = array(
            'url' => get_page_link(256),
            'text' => 'Collezioni',
        );

        array_splice( $links, 1, -2, $breadcrumb );
    }

    return $links;
}


// GESTIONE UTENTI CAMPI NASCOSTI
function remove_personal_options(){
    echo '<script type="text/javascript">jQuery(document).ready(function($) {
  
$(\'#your-profile .yoast-settings\').remove();
$(\'.user-googleplus-wrap\').remove();
$(\'.user-twitter-wrap\').remove();
$(\'.user-facebook-wrap\').remove();
$(\'.user-pinterest-wrap\').remove();
$(\'.user-bitbucket-wrap\').remove();
$(\'.user-github-wrap\').remove();
$(\'.user-telegram-wrap\').remove();
$(\'.user-comment-shortcuts-wrap\').remove();
$(\'.user-instagram-wrap\').remove();
$(\'.user-language-wrap\').remove();
 
});</script>';

}

add_action('admin_head','remove_personal_options');


// Disable email notification change password
if ( !function_exists( 'wp_password_change_notification' ) ) {
    function wp_password_change_notification() {}
}


// Dynamic url Custom Post Type Collezioni
function my_msls_options_get_permalink( $url, $language ) {
    if ( 'fr_FR' == $language ) {
        $url = str_replace( '/arredamento-ufficio-di-design/', '/mobilier-de-bureau-design/', $url );
        $url = str_replace( '/design-office-furniture/', '/mobilier-de-bureau-design/', $url );
    }
    elseif ( 'it_IT' == $language ) {
        $url = str_replace( '/design-office-furniture/', '/arredamento-ufficio-di-design/', $url );
        $url = str_replace( '/mobilier-de-bureau-design/', '/arredamento-ufficio-di-design/', $url );
    }
    elseif ( 'en_US' == $language ) {
        $url = str_replace( '/arredamento-ufficio-di-design/', '/design-office-furniture/', $url );
        $url = str_replace( '/mobilier-de-bureau-design/', '/design-office-furniture/', $url );
    }
    return $url;
}
add_filter( 'msls_options_get_permalink', 'my_msls_options_get_permalink', 10, 2 );





// CUSTOM BACKEND

// rimuovi notifiche
if ( !current_user_can( 'edit_users' ) ) {
    add_filter('pre_site_transient_update_core', create_function('$a', "return null;"));
    add_filter('pre_site_transient_update_plugins', create_function( '$a', "return null;"));
}

add_filter( 'admin_footer_text', 'my_admin_footer_text' );
function my_admin_footer_text( $default_text ) {
    return '<span>Design by <a href="http://www.dexanet.com" target="_blank" title="realizzazione siti internet brescia">Dexanet</a>';
}
// logo login
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/blog-logo.png);
            padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// alt del link del logo del login
add_filter("login_headertitle","customized_login_title");
function customized_login_title($message){
    return "Blog Collection - Sesta";
}
// link al sito del cliente
add_filter("login_headerurl","customized_login_url");
function customized_login_url($url){
    return get_bloginfo('url');
}

// Disable wp-admin except for Admin



// AREA RISERVATA
// CUSTOM USER ROLE DESIGNER
$result = add_role( 'designer', __(
    'Designer' ),
    array(
        'read' => true, // true allows this capability
        'edit_posts' => false, // Allows user to edit their own posts
        'edit_pages' => false, // Allows user to edit pages
        'edit_others_posts' => false, // Allows user to edit others posts not just their own
        'create_posts' => false, // Allows user to create new posts
        'manage_categories' => false, // Allows user to manage post categories
        'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
        'edit_themes' => false, // false denies this capability. User can’t edit your theme
        'install_plugins' => false, // User cant add new plugins
        'update_plugin' => false, // User can’t update any plugins
        'update_core' => false // user cant perform core updates
    )
);

// CUSTOM USER ROLE
$result = add_role( 'aziende', __(
    'Aziende' ),
    array(
        'read' => true, // true allows this capability
        'edit_posts' => false, // Allows user to edit their own posts
        'edit_pages' => false, // Allows user to edit pages
        'edit_others_posts' => false, // Allows user to edit others posts not just their own
        'create_posts' => false, // Allows user to create new posts
        'manage_categories' => false, // Allows user to manage post categories
        'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
        'edit_themes' => false, // false denies this capability. User can’t edit your theme
        'install_plugins' => false, // User cant add new plugins
        'update_plugin' => false, // User can’t update any plugins
        'update_core' => false // user cant perform core updates
    )
);

function cc_wpse_278096_disable_admin_bar() {
    if (current_user_can('administrator') || current_user_can('contributor') ) {
        // user can view admin bar
        show_admin_bar(true); // this line isn't essentially needed by default...
    } else {
        // hide admin bar
        show_admin_bar(false);
    }
}
add_action('after_setup_theme', 'cc_wpse_278096_disable_admin_bar');


// Gestione Utenti
/**
 * Redirect to the custom login page
 */
function cubiq_login_init () {
    $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'login';

    if ( isset( $_POST['wp-submit'] ) ) {
        $action = 'post-data';
    } else if ( isset( $_GET['reauth'] ) ) {
        $action = 'reauth';
    }

    // redirect to change password form
    if ( $action == 'rp' || $action == 'resetpass' ) {
        if( isset($_GET['key']) && isset($_GET['login']) ) {
            $rp_path = wp_unslash('/user-login/');
            $rp_cookie	= 'wp-resetpass-' . COOKIEHASH;
            $value = sprintf( '%s:%s', wp_unslash( $_GET['login'] ), wp_unslash( $_GET['key'] ) );
            setcookie( $rp_cookie, $value, 0, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
        }

        wp_redirect( home_url('/user-login/?action=resetpass') );
        exit;
    }

    // redirect from wrong key when resetting password
    if ( $action == 'lostpassword' && isset($_GET['error']) && ( $_GET['error'] == 'expiredkey' || $_GET['error'] == 'invalidkey' ) ) {
        wp_redirect( home_url( '/user-login/?action=forgot&failed=wrongkey' ) );
        exit;
    }

    if (
        $action == 'post-data'		||			// don't mess with POST requests
        $action == 'reauth'			||			// need to reauthorize
        $action == 'logout'						// user is logging out
    ) {
        return;
    }

    wp_redirect( home_url( '/user-login/' ) );
    exit;
}
add_action('login_init', 'cubiq_login_init');


/**
 * Redirect logged in users to the right page
 */
function cubiq_template_redirect () {
    if ( is_page( 'user-login' ) && is_user_logged_in() ) {
        wp_redirect( home_url( '/user/' ) );
        exit();
    }

    if ( is_page( 'user' ) && !is_user_logged_in() ) {
        wp_redirect( home_url( '/user-login/' ) );
        exit();
    }
}
add_action( 'template_redirect', 'cubiq_template_redirect' );


/**
 * Prevent subscribers to access the admin
 */
/*function cubiq_admin_init () {

    if ( current_user_can( 'subscriber' ) && !defined( 'DOING_AJAX' ) ) {
        wp_redirect( home_url('/user/') );
        exit;
    }

}
add_action( 'admin_init', 'cubiq_admin_init' );*/


/**
 * Registration page redirect
 */
function cubiq_registration_redirect ($errors, $sanitized_user_login, $user_email) {

    // don't lose your time with spammers, redirect them to a success page
    if ( !isset($_POST['confirm_email']) || $_POST['confirm_email'] !== '' ) {

        wp_redirect( home_url('/user-login/') . '?action=register&success=1' );
        exit;

    }

    if ( !empty( $errors->errors) ) {
        if ( isset( $errors->errors['username_exists'] ) ) {

            wp_redirect( home_url('/user-login/') . '?action=register&failed=username_exists' );

        } else if ( isset( $errors->errors['email_exists'] ) ) {

            wp_redirect( home_url('/user-login/') . '?action=register&failed=email_exists' );

        } else if ( isset( $errors->errors['invalid_username'] ) ) {

            wp_redirect( home_url('/user-login/') . '?action=register&failed=invalid_username' );

        } else if ( isset( $errors->errors['invalid_email'] ) ) {
            +
            +			wp_redirect( home_url('/user-login/') . '?action=register&failed=invalid_email' );

        } else if ( isset( $errors->errors['empty_username'] ) || isset( $errors->errors['empty_email'] ) ) {

            wp_redirect( home_url('/user-login/') . '?action=register&failed=empty' );

        } else {

            wp_redirect( home_url('/user-login/') . '?action=register&failed=generic' );

        }

        exit;
    }

    return $errors;

}
add_filter('registration_errors', 'cubiq_registration_redirect', 10, 3);


/**
 * Login page redirect
 */
function cubiq_login_redirect ($redirect_to, $url, $user) {

    if ( !isset($user->errors) ) {
        return $redirect_to;
    }

    wp_redirect( home_url('/user-login/') . '?action=login&failed=1');
    exit;

}
add_filter('login_redirect', 'cubiq_login_redirect', 10, 3);


$blog_id = get_current_blog_id();

if ( 1 == $blog_id ) {
    // EMAIL
    add_filter('password_change_email', 'change_password_mail_message', 10, 3 );
    function change_password_mail_message( $pass_change_mail, $user, $userdata ) {
        $new_message_txt = __( 'Ciao ###USERNAME###,
  la tua iscrizione è stata confermata.
  Puoi eseguire il login con le tue credenziali dedicate:
  User: ###USERNAME### .
  Riceverai la tua password in una seconda email.');
        $pass_change_mail[ 'subject' ] = 'Registrazione confermata';
        $pass_change_mail[ 'message' ] = $new_message_txt;
        return $pass_change_mail;
    }
} if ( 2 == $blog_id ) {
    // EMAIL
    add_filter('password_change_email', 'change_password_mail_message', 10, 3 );
    function change_password_mail_message( $pass_change_mail, $user, $userdata ) {
        $new_message_txt = __( 'Hello ###USERNAME###,
  your registration has been confirmed.
  You can login with your dedicated credentials:
  User: ###USERNAME### .
  You will receive your password in a second email.');
        $pass_change_mail[ 'subject' ] = 'Registration confirmed';
        $pass_change_mail[ 'message' ] = $new_message_txt;
        return $pass_change_mail;
    }
} if ( 3 == $blog_id ) {
// EMAIL
    add_filter('password_change_email', 'change_password_mail_message', 10, 3 );
    function change_password_mail_message( $pass_change_mail, $user, $userdata ) {
        $new_message_txt = __( 'Bonjour ###USERNAME###,
  votre inscription a été confirmée.
  Vous pouvez vous connecter avec vos informations d\'identification dédiées:
  Utilisateur: ###USERNAME### .
  Vous recevrez votre mot de passe dans un deuxième courriel.');
        $pass_change_mail[ 'subject' ] = 'Enregistrement confirmé';
        $pass_change_mail[ 'message' ] = $new_message_txt;
        return $pass_change_mail;
    }
}





