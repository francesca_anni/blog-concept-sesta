<?php
    if($_GET["tab"] == "seo-options"){
        add_settings_section("seo_section", "Impostazioni", "display_seo_options_content", "theme-options");
        // SEo
        add_settings_field(
            "ga_seo",
            "Google Analytics ID",
            "display_seo_ga_element",
            "theme-options",
            "seo_section"
        );
        add_settings_field(
            "seo_tgm",
            "Google Tag Manager ID",
            "display_seo_tgm_element",
            "theme-options",
            "seo_section"
        );
        add_settings_field(
            "seo_hjar",
            "Hotjar Analytics ID",
            "display_seo_hjar_element",
            "theme-options",
            "seo_section"
        );
        add_settings_field(
            "seo_ads",
            "Adwords remarketing tag",
            "display_seo_ads_element",
            "theme-options",
            "seo_section"
        );
        add_settings_field(
            "facebook_pixel",
            "Facebook Pixel",
            "display_facebook_pixel_element",
            "theme-options",
            "seo_section"
        );
    }
?>