<?php 
    add_settings_section("email_section", "Impostazioni", "display_smtp_header", "theme-options");
    // SMTP Options
    add_settings_field(
        "enable_smtp",
        "SMTP",
        "display_smtp",
        "theme-options",
        "email_section"
    );
    add_settings_field(
        "host_smtp",
        "Host",
        "display_host_smtp",
        "theme-options",
        "email_section"
    );
    add_settings_field(
        "auth_smtp",
        "SMTP Auth",
        "display_auth_smtp",
        "theme-options",
        "email_section"
    );
    add_settings_field(
        "port_smtp",
        "SMTP Port",
        "display_port_smtp",
        "theme-options",
        "email_section"
    );
    add_settings_field(
        "user_smtp",
        "SMTP User",
        "display_user_smtp",
        "theme-options",
        "email_section"
    );
    add_settings_field(
        "pass_smtp",
        "SMTP Password",
        "display_pass_smtp",
        "theme-options",
        "email_section"
    );
    add_settings_field(
        "secure_smtp",
        "SMTP Sicurezza",
        "display_secure_smtp",
        "theme-options",
        "email_section"
    );
    add_settings_field(
        "from_smtp",
        "SMTP Da",
        "display_from_smtp",
        "theme-options",
        "email_section"
    );
    add_settings_field(
        "name_smtp",
        "SMTP Nome Mittente",
        "display_name_smtp",
        "theme-options",
        "email_section"
    );
?>