<?php
    add_settings_section("footer_section", "Impostazioni", "display_footer_options_content", "theme-options");
    // Footer
    add_settings_field(
        "footer_text",
        "Copyright footer",
        "display_footer_element",
        "theme-options",
        "footer_section"
    );
?>