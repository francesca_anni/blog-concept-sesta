<?php
    add_settings_section("social_section", "Impostazioni", "display_seo_options_content", "theme-options");
    // Social
    add_settings_field(
        "twitter_url",
        "Twitter Profile Url",
        "display_twitter_element",
        "theme-options",
        "social_section"
    );
    add_settings_field(
        "facebook_url",
        "Facebook Profile Url",
        "display_facebook_element",
        "theme-options",
        "social_section"
    );
    add_settings_field(
        "googleplus_url",
        "Google+ Profile Url",
        "display_googleplus_element",
        "theme-options",
        "social_section"
    );
    add_settings_field(
        "instagram_url",
        "Instagram Profile Url",
        "display_instagram_element",
        "theme-options",
        "social_section"
    );
    add_settings_field(
        "pinterest_url",
        "Pinterest Profile Url",
        "display_pinterest_element",
        "theme-options",
        "social_section"
    );
?>