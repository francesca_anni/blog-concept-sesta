<?php 
    add_settings_section("cookie_section", "Impostazioni", "display_cookie_header", "theme-options");
    // Cookie Options
    add_settings_field(
        "enable_cookie",
        "Avviso cookie",
        "display_cookie",
        "theme-options",
        "cookie_section"
    );
    add_settings_field(
        "cookie_text",
        "Inserisci il testo dei cookie",
        "display_cookie_text",
        "theme-options",
        "cookie_section"
    );
    add_settings_field(
        "cookie_accept_text",
        "Inserisci il testo del pulsante",
        "display_cookie_accept_text",
        "theme-options",
        "cookie_section"
    );
    add_settings_field(
        "cookie_text_link",
        "Inserisci il testo del link alla pagina",
        "display_cookie_link_text",
        "theme-options",
        "cookie_section"
    );
    add_settings_field(
        "cookie_link",
        "Inserisci il link alla pagina cookie",
        "display_cookie_link",
        "theme-options",
        "cookie_section"
    );
    add_settings_field(
        "cookie_color_settings",
        "Seleziona la posizione e il colore",
        "cookie_color_select_field_0_render",
        "theme-options",
        "cookie_section"
    );
?>