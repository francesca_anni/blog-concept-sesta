<?php
    add_settings_section("google_section", "Impostazioni", "display_google_options_content", "theme-options");
    // Google
    add_settings_field(
        "googlemaps_key",
        "Google Maps API KEY",
        "display_googlemaps_element",
        "theme-options",
        "google_section"
    );
    add_settings_field(
        "googlefont_url",
        "Google Font",
        "display_googlefont_element",
        "theme-options",
        "google_section"
    );
    add_settings_field(
        "googlefont_2_url",
        "Google Font 2",
        "display_googlefont_2_element",
        "theme-options",
        "google_section"
    );
    add_settings_field(
        "googlefont_3_url",
        "Google Font 3",
        "display_googlefont_3_element",
        "theme-options",
        "google_section"
    );
?>