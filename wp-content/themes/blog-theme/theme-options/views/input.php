<?php
    function display_logo_form_element(){?>
    <input placeholder="https://example.com/img.jpg" type="text" name="header_logo" id="header_logo" value="<?php echo get_option('header_logo'); ?>" />
        <?php
    }
    function display_logo_secondary_form_element(){?>
        <input placeholder="https://example.com/img.jpg" type="text" name="header_logo_secondary" id="header_logo_secondary" value="<?php echo get_option('header_logo_secondary'); ?>" />
        <?php
    }
    function display_logo_alt_form_element(){?>
        <input placeholder="Alt text" type="text" name="header_logo_alt" id="header_logo_alt" value="<?php echo get_option('header_logo_alt'); ?>" />
        <?php
    }
    /** SEO Options**/
    function display_seo_ga_element(){ ?>
        <input placeholder="GAXXXX-XX" type="text" name="ga_seo" id="ga_seo" value="<?php echo get_option('ga_seo'); ?>" />
        <?php
    }
	function display_seo_tgm_element(){ ?>
        <input placeholder="TMXXXX-XX" type="text" name="seo_tgm" id="seo_tgm" value="<?php echo get_option('seo_tgm'); ?>" />
        <?php
    }
	function display_seo_hjar_element(){ ?>
        <input type="text" name="seo_hjar" id="seo_tgm" value="<?php echo get_option('seo_hjar'); ?>" />
        <?php
    }
    function display_seo_ads_element(){ ?>
        <textarea rows='7' cols='50' name="seo_ads" id="seo_ads"><?php echo get_option('seo_ads'); ?></textarea>
        <?php
    }
    function display_facebook_pixel_element(){ ?>
        <textarea rows='7' cols='50' name="facebook_pixel" id="facebook_pixel"><?php echo get_option('facebook_pixel'); ?></textarea>
        <?php
    }
    /**Theme Options **/
    function display_layout_element_fontawesome(){ ?>
        <input type="checkbox" name="font_awesome" value="1" <?php checked(1, get_option('font_awesome'), true); ?> />
        <label>Demo: http://fontawesome.io/icons/</label>
        <?php
    }
    function display_layout_element_animatecss(){ ?>
        <input type="checkbox" name="animate_css" value="1" <?php checked(1, get_option('animate_css'), true); ?> />
        <label>Demo: https://daneden.github.io/animate.css/</label>
        <?php
    }
    function display_layout_element_hover_css(){ ?>
        <input type="checkbox" name="hover_css" value="1" <?php checked(1, get_option('hover_css'), true); ?> />
        <label>Demo: http://ianlunn.github.io/Hover/</label>
        <?php
    }
    function display_layout_element(){ ?>
        <input type="checkbox" name="theme_layout" value="1" <?php checked(1, get_option('theme_layout'), true); ?> />
        <?php
    }
	function display_layout_element_menu(){ ?>
        <input type="checkbox" name="menu_layout" value="1" <?php checked(1, get_option('menu_layout'), true); ?> />
        <?php
    }
    function menu_position_select_field_0_render(){
        $options_menu = get_option('menu_position_settings');
        ?>
        <select name='menu_position_settings[menu_position_select_field_0]' id="menu_position_select_field_0">
            <option value='' <?php echo selected( $options_menu['menu_position_select_field_0'], '', false ); ?>>Default</option>
            <option value='fixed-top' <?php echo selected( $options_menu['menu_position_select_field_0'], 'fixed-top', false ); ?>>Fixed Top</option>
            <option value='fixed-bottom' <?php echo selected( $options_menu['menu_position_select_field_0'], 'fixed-bottom', false ); ?>>Fixed Bottom</option>
            <option value='sticky-top' <?php echo selected( $options_menu['menu_position_select_field_0'], 'sticky-top', false ); ?>>Sticky Top</option>
        </select>
        <?php
    }
	function display_layout_element_footer(){ ?>
        <input type="checkbox" name="footer_layout" value="1" <?php checked(1, get_option('footer_layout'), true); ?> />
        <?php
    }
    function display_color_statusbar(){?>
        <input placeholder="#000000" type="text" name="statusbar_color" id="statusbar_color" value="<?php echo get_option('statusbar_color'); ?>" />
        <?php
    }
    /** Cookie tab **/
    function display_cookie(){ ?>
        <input type="checkbox" name="enable_cookie" value="1" <?php checked(1, get_option('enable_cookie'), true); ?> />
        <label>Vuoi abilitare l'avviso dei cookie?</label>
        <?php
    }
    function display_cookie_text(){ ?>
        <textarea rows='7' cols='50' name="cookie_text" id="cookie_text"><?php echo get_option('cookie_text'); ?></textarea>
        <?php
    }
    function display_cookie_accept_text(){?>
        <input placeholder="Accetto" type="text" name="cookie_accept_text" id="cookie_accept_text" value="<?php echo get_option('cookie_accept_text'); ?>" />
        <?php
    }
    function display_cookie_link_text(){ ?>
        <textarea rows='2' cols='50' placeholder="Leggi la nostra cookie policy" name="cookie_text_link" id="cookie_text_link"><?php echo get_option('cookie_text_link'); ?></textarea>
        <?php
    }
    function display_cookie_link(){?>
        <input placeholder="/cookie-page/" type="text" name="cookie_link" id="cookie_link" value="<?php echo get_option('cookie_link'); ?>" />
        <?php
    }
    function cookie_color_select_field_0_render(){ 
        $options = get_option('cookie_color_settings');
        ?>
        <select name='cookie_color_settings[cookie_color_select_field_0]' id="cookie_color_select_field_0">
            <option value='light-top' <?php echo selected( $options['cookie_color_select_field_0'], 'light-top', false ); ?>>Light Top</option>
            <option value='light-bottom' <?php echo selected( $options['cookie_color_select_field_0'], 'light-bottom', false ); ?>>Light Bottom</option>
            <option value='dark-bottom' <?php echo selected( $options['cookie_color_select_field_0'], 'dark-bottom', false ); ?>>Dark Bottom</option>
            <option value='dark-top' <?php echo selected( $options['cookie_color_select_field_0'], 'dark-top', false ); ?>>Dark Top</option>
        </select>
    <?php
    }
    /** Contact **/
    function display_contact_rs_element(){ ?>
        <input type="text" placeholder="S.r.l" name="contact_ragione_sociale" id="contact_ragione_sociale" value="<?php echo get_option('contact_ragione_sociale'); ?>" />
        <?php
    }
    function display_contact_indirizzo_element(){ ?>
        <textarea rows='7' cols='50' name="contact_indirizzo" id="contact_indirizzo"><?php echo get_option('contact_indirizzo'); ?></textarea>
        <?php
    }
    function display_contact_telefono_element(){ ?>
        <input type="text" placeholder="+00 333555 999" name="contact_telefono" id="contact_telefono" value="<?php echo get_option('contact_telefono'); ?>" />
        <?php
    }
    function display_contact_fax_element(){ ?>
        <input type="text" placeholder="+00 333555 999" name="contact_fax" id="contact_fax" value="<?php echo get_option('contact_fax'); ?>" />
        <?php
    }
    function display_contact_mail_element(){ ?>
        <input type="text" placeholder="example@dexanet.com" name="contact_mail" id="contact_mail" value="<?php echo get_option('contact_mail'); ?>" />
        <?php
    }
    function display_contact_vat_number_element(){ ?>
        <input type="text" placeholder="012345678910" name="contact_vat_number" id="contact_vat_number" value="<?php echo get_option('contact_vat_number'); ?>" />
        <?php
    }
    function display_contact_orari_element(){ ?>
        <textarea rows='7' cols='50' name="contact_orari" id="contact_orari"><?php echo get_option('contact_orari'); ?></textarea>
        <?php
    }

	/** Social Link **/
	function display_twitter_element(){ ?>
        <input placeholder="https://twitter.com/" type="text" name="twitter_url" id="twitter_url" value="<?php echo get_option('twitter_url'); ?>" />
        <?php
    }
	function display_facebook_element(){ ?>
        <input placeholder="https://facebook.com/" type="text" name="facebook_url" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>" />
        <?php
    }
    function display_googleplus_element(){ ?>
        <input placeholder="https://googleplus.com/" type="text" name="googleplus_url" id="googleplus_url" value="<?php echo get_option('googleplus_url'); ?>" />
        <?php
    }
    function display_instagram_element(){ ?>
        <input placeholder="https://instagram.com/" type="text" name="instagram_url" id="instagram_url" value="<?php echo get_option('instagram_url'); ?>" />
        <?php
    }
    function display_pinterest_element(){ ?>
        <input placeholder="https://pinterest.com/" type="text" name="pinterest_url" id="pinterest_url" value="<?php echo get_option('pinterest_url'); ?>" />
        <?php
    }
    function display_googlemaps_element(){ ?>
        <input type="text" placeholder="XXXXXX-XX" name="googlemaps_key" id="googlemaps_key" value="<?php echo get_option('googlemaps_key'); ?>" />
        <?php
    }
    function display_googlefont_element(){ ?>
        <input placeholder="https://fonts.google.com/" type="text" name="googlefont_url" id="googlefont_url" value="<?php echo get_option('googlefont_url'); ?>" />
        <?php
    }
    function display_googlefont_2_element(){ ?>
        <input placeholder="https://fonts.google.com/" type="text" name="googlefont_2_url" id="googlefont_2_url" value="<?php echo get_option('googlefont_2_url'); ?>" />
        <?php
    }
    function display_googlefont_3_element(){ ?>
        <input placeholder="https://fonts.google.com/" type="text" name="googlefont_3_url" id="googlefont_3_url" value="<?php echo get_option('googlefont_3_url'); ?>" />
        <?php
    }
    function display_footer_element(){ ?>
        <textarea rows='7' cols='50' name="footer_text" id="footer_text"><?php echo get_option('footer_text'); ?></textarea>
        <?php
    }
    /** Email SMTP **/
    function display_smtp(){ ?>
        <input type="checkbox" name="enable_smtp" value="1" <?php checked(1, get_option('enable_smtp'), true); ?> />
        <label>Vuoi abilitare le mail in SMTP?</label>
        <?php
    }
    function display_host_smtp(){ ?>
        <input placeholder="stmp.dexnet.com" type="text" name="host_smtp" id="host_smtp" value="<?php echo get_option('host_smtp'); ?>" />
        <?php
    }
    function display_auth_smtp(){ ?>
        <input placeholder="true or false" type="text" name="auth_smtp" id="auth_smtp" value="<?php echo get_option('auth_smtp'); ?>" />
        <?php
    }
    function display_port_smtp(){ ?>
        <input placeholder="587" type="text" name="port_smtp" id="port_smtp" value="<?php echo get_option('port_smtp'); ?>" />
        <?php
    }
    function display_user_smtp(){ ?>
        <input placeholder="test@test.it" type="text" name="user_smtp" id="user_smtp" value="<?php echo get_option('user_smtp'); ?>" />
        <?php
    }
    function display_pass_smtp(){ ?>
        <input placeholder="password" type="password" name="pass_smtp" id="pass_smtp" value="<?php echo get_option('pass_smtp'); ?>" />
        <?php
    }
    function display_secure_smtp(){ ?>
        <input placeholder="ssl or tls" type="text" name="secure_smtp" id="secure_smtp" value="<?php echo get_option('secure_smtp'); ?>" />
        <?php
    }
    function display_from_smtp(){ ?>
        <input placeholder="cliente@cliente.it" type="text" name="from_smtp" id="from_smtp" value="<?php echo get_option('from_smtp'); ?>" />
        <?php
    }
    function display_name_smtp(){ ?>
        <input placeholder="Nome azienda s.r.l" type="text" name="name_smtp" id="name_smtp" value="<?php echo get_option('name_smtp'); ?>" />
        <?php
    }
    ?>