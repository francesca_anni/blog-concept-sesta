<?php
register_setting("seo_section", "ga_seo");
register_setting("seo_section", "seo_tgm");
register_setting("seo_section", "seo_hjar");
register_setting("seo_section", "seo_ads");
register_setting("seo_section", "facebook_pixel");

register_setting("social_section", "twitter_url");
register_setting("social_section", "facebook_url");
register_setting("social_section", "googleplus_url");
register_setting("social_section", "instagram_url");
register_setting("social_section", "pinterest_url");

register_setting("google_section", "googlemaps_key");
register_setting("google_section", "googlefont_url");
register_setting("google_section", "googlefont_2_url");
register_setting("google_section", "googlefont_3_url");

register_setting("footer_section", "footer_text");

register_setting("header_section", "font_awesome");
register_setting("header_section", "animate_css");
register_setting("header_section", "hover_css");
register_setting("header_section", "header_logo");
register_setting("header_section", "header_logo_secondary");
register_setting("header_section", "header_logo_alt");
register_setting("header_section", "theme_layout");
register_setting("header_section", "menu_layout");
register_setting("header_section", "menu_position_settings");
register_setting("header_section", "statusbar_color");
register_setting("header_section", "footer_layout");

register_setting("contact_section", "contact_ragione_sociale");
register_setting("contact_section", "contact_indirizzo");
register_setting("contact_section", "contact_telefono");
register_setting("contact_section", "contact_fax");
register_setting("contact_section", "contact_mail");
register_setting("contact_section", "contact_vat_number");
register_setting("contact_section", "contact_orari");

register_setting("cookie_section", "enable_cookie");
register_setting("cookie_section", "cookie_text");
register_setting("cookie_section", "cookie_accept_text");
register_setting("cookie_section", "cookie_text_link");
register_setting("cookie_section", "cookie_link");
register_setting("cookie_section", "cookie_color_settings");

register_setting("email_section", "enable_smtp");
register_setting("email_section", "host_smtp");
register_setting("email_section", "auth_smtp");
register_setting("email_section", "port_smtp");
register_setting("email_section", "user_smtp");
register_setting("email_section", "pass_smtp");
register_setting("email_section", "secure_smtp");
register_setting("email_section", "from_smtp");
register_setting("email_section", "name_smtp");
?>