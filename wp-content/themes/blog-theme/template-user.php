<?php
/*
Template Name: Template User
*/
?>

<?php

global $current_user;
get_currentuserinfo();

require_once( ABSPATH . WPINC . '/registration.php' );

if ( !empty($_POST) && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

	/* Update user password */
	if ( !empty($_POST['current_pass']) && !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {

		if ( !wp_check_password( $_POST['current_pass'], $current_user->user_pass, $current_user->ID) ) {
			$error = 'Your current password does not match. Please retry.';
		} elseif ( $_POST['pass1'] != $_POST['pass2'] ) {
			$error = 'The passwords do not match. Please retry.';
		} elseif ( strlen($_POST['pass1']) < 4 ) {
			$error = 'A bit short as a password, don\'t you thing?';
		} elseif ( false !== strpos( wp_unslash($_POST['pass1']), "\\" ) ) {
			$error = 'Password may not contain the character "\\" (backslash).';
		} else {
			$error = wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );

			if ( !is_int($error) ) {
				$error = 'An error occurred while updating your profile. Please retry.';
			} else {
				$error = false;
			}
		}

		if ( empty($error) ) {
			do_action('edit_user_profile_update', $current_user->ID);
			wp_redirect( site_url('/user/') . '?success=1' );
			exit;
		}
	}
}

?>

<section class="tit-page">
    <div class="container">
        <div class="row">
            <div class="col">
	            <?php while ( have_posts() ) : the_post(); ?>
                    <article id="page-<?php the_ID(); ?>" class="meta-box hentry">
                        <div class="post-content cf">
                            <?php if ( !empty($_GET['success']) ): ?>
                                <div class="message-box message-success">
                                    <span class="icon-thumbs-up"></span>
                                    Profile updated successfully!
                                </div>
                            <?php endif; ?>

                            <?php if ( !empty($error) ): ?>
                                <div class="message-box message-error">
                                    <span class="icon-thumbs-up"></span>
                                    <?php echo $error; ?>
                                </div>
                            <?php endif; ?>
                            <header class="entry-header">
                                <?php
                                $blog_id = get_current_blog_id();
                                if ( 1 == $blog_id ) { ?>
                                    <h1 class="entry-title">Benvenuto, <span class="userColor"><?php echo esc_html($current_user->display_name); ?></span></h1>
                                <?php } if ( 2 == $blog_id ) { ?>
                                    <h1 class="entry-title">Welcome, <span class="userColor"><?php echo esc_html($current_user->display_name); ?></span></h1>
                                <?php } if ( 3 == $blog_id ) { ?>
                                    <h1 class="entry-title">Bienvenue, <span class="userColor"><?php echo esc_html($current_user->display_name); ?></span></h1>
                                <?php } ?>
                            </header>
                            <div class="entry-content">
                                <?php
                                $blog_id = get_current_blog_id();
                                if ( 1 == $blog_id ) { ?>
                                    <h3>Benvenuti nell'Area Riservata di Sesta.</h3>
                                <?php } if ( 2 == $blog_id ) { ?>
                                    <h3>Welcome to the Reserved Area of the Blog.</h3>
                                <?php } if ( 3 == $blog_id ) { ?>
                                    <h3>Bienvenue dans la Zone Réservée de Blog.</h3>
                                <?php } ?>
                            </div>
                            <hr>
                            <p><a style="float:right" href="<?php echo wp_logout_url( home_url() ); ?>" class="icon-cancel standard-button button-logout">logout</a></p>
                        </div>
                    </article>
	            <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>


