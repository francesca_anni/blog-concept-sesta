<?php
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
/**
 * Theme activation
 */
if (is_admin() && isset($_GET['activated']) && 'themes.php' == $GLOBALS['pagenow']) {
  wp_redirect(admin_url('themes.php?page=theme_activation_options'));
  exit;
}
function mad_theme_activation_options_init() {
  register_setting(
    'mad_activation_options',
    'mad_theme_activation_options'
  );
}
add_action('admin_init', 'mad_theme_activation_options_init');

function mad_activation_options_page_capability() {
  return 'edit_theme_options';
}
add_filter('option_page_capability_mad_activation_options', 'mad_activation_options_page_capability');

function mad_theme_activation_options_add_page() {
  $mad_activation_options = mad_get_theme_activation_options();

  if (!$mad_activation_options) {
    add_theme_page(
      __('Theme Activation', 'mad'),
      __('Theme Activation', 'mad'),
      'edit_theme_options',
      'theme_activation_options',
      'mad_theme_activation_options_render_page'
    );
  } else {
    if (is_admin() && isset($_GET['page']) && $_GET['page'] === 'theme_activation_options') {
      flush_rewrite_rules();
      wp_redirect(admin_url('themes.php'));
      exit;
    }
  }
}
add_action('admin_menu', 'mad_theme_activation_options_add_page', 50);

function mad_get_theme_activation_options() {
  return get_option('mad_theme_activation_options');
}

function mad_theme_activation_options_render_page() { ?>
  <div class="wrap">
    <h2><?php printf(__('%s Theme Activation', 'mad'), wp_get_theme()); ?></h2>
    <div class="update-nag">
      <?php _e('These settings are optional and should usually be used only on a fresh installation', 'mad'); ?>
    </div>
    <?php settings_errors(); ?>

    <form method="post" action="options.php">
      <?php settings_fields('mad_activation_options'); ?>
      <table class="form-table">
        <tr valign="top"><th scope="row"><?php _e('Create static front page?', 'mad'); ?></th>
          <td>
            <fieldset>
              <legend class="screen-reader-text"><span><?php _e('Create static front page?', 'mad'); ?></span></legend>
              <select name="mad_theme_activation_options[create_front_page]" id="create_front_page">
                <option selected="selected" value="true"><?php echo _e('Yes', 'mad'); ?></option>
                <option value="false"><?php echo _e('No', 'mad'); ?></option>
              </select>
              <p class="description"><?php printf(__('Create a page called Home and set it to be the static front page', 'mad')); ?></p>
            </fieldset>
          </td>
        </tr>
        <tr valign="top"><th scope="row"><?php _e('Create additional pages?', 'mad'); ?></th>
          <td>
            <fieldset>
              <legend class="screen-reader-text"><span><?php _e('Create additional pages?', 'mad'); ?></span></legend>
              <select name="mad_theme_activation_options[create_additional_page]" id="create_additional_page">
                <option selected="selected" value="true"><?php echo _e('Yes', 'mad'); ?></option>
                <option value="false"><?php echo _e('No', 'mad'); ?></option>
              </select>
              <p class="description"><?php printf(__('Create additional pages (Contatti, Privacy, etc)', 'mad')); ?></p>
            </fieldset>
          </td>
        </tr>
        <tr valign="top"><th scope="row"><?php _e('Change permalink structure?', 'mad'); ?></th>
          <td>
            <fieldset>
              <legend class="screen-reader-text"><span><?php _e('Update permalink structure?', 'mad'); ?></span></legend>
              <select name="mad_theme_activation_options[change_permalink_structure]" id="change_permalink_structure">
                <option selected="selected" value="true"><?php echo _e('Yes', 'mad'); ?></option>
                <option value="false"><?php echo _e('No', 'mad'); ?></option>
              </select>
              <p class="description"><?php printf(__('Change permalink structure to /&#37;postname&#37;/', 'mad')); ?></p>
            </fieldset>
          </td>
        </tr>
        <tr valign="top"><th scope="row"><?php _e('Create navigation menu?', 'mad'); ?></th>
          <td>
            <fieldset>
              <legend class="screen-reader-text"><span><?php _e('Create navigation menu?', 'mad'); ?></span></legend>
              <select name="mad_theme_activation_options[create_navigation_menus]" id="create_navigation_menus">
                <option selected="selected" value="true"><?php echo _e('Yes', 'mad'); ?></option>
                <option value="false"><?php echo _e('No', 'mad'); ?></option>
              </select>
              <p class="description"><?php printf(__('Create the Primary Navigation menu and set the location', 'mad')); ?></p>
            </fieldset>
          </td>
        </tr>
        <tr valign="top"><th scope="row"><?php _e('Add pages to menu?', 'mad'); ?></th>
          <td>
            <fieldset>
              <legend class="screen-reader-text"><span><?php _e('Add pages to menu?', 'mad'); ?></span></legend>
              <select name="mad_theme_activation_options[add_pages_to_primary_navigation]" id="add_pages_to_primary_navigation">
                <option selected="selected" value="true"><?php echo _e('Yes', 'mad'); ?></option>
                <option value="false"><?php echo _e('No', 'mad'); ?></option>
              </select>
              <p class="description"><?php printf(__('Add all current published pages to the Primary Navigation', 'mad')); ?></p>
            </fieldset>
          </td>
        </tr>
      </table>
      <?php submit_button(); ?>
    </form>
  </div>

<?php }

function mad_theme_activation_action() {

  if (!($mad_theme_activation_options = mad_get_theme_activation_options())) {
    return;
  }

  if (strpos(wp_get_referer(), 'page=theme_activation_options') === false) {
    return;
  }
  
  if ($mad_theme_activation_options['create_additional_page'] === 'true') {
    $mad_theme_activation_options['create_additional_page'] = false;
    $default_pages[] =  __('Contatti', 'mad');
    $default_pages[] =  __('Privacy and Policy', 'mad');
  }

  if ($mad_theme_activation_options['create_front_page'] === 'true') {
    $mad_theme_activation_options['create_front_page'] = false;

    $default_pages[] = __('Home', 'mad');
    // $default_pages = array(__('Home', 'mad'));
    $existing_pages = get_pages();
    $temp = array();

    foreach ($existing_pages as $page) {
      $temp[] = $page->post_title;
    }

    $pages_to_create = array_diff($default_pages, $temp);

    foreach ($pages_to_create as $new_page_title) {
        if ($new_page_title == 'Privacy and Policy') {
            $content = '<h1>Privacy Policy di [www.nomesito.com]</h1>
Questa Applicazione raccoglie alcuni Dati Personali dei propri Utenti.
<h2>Titolare del Trattamento dei Dati</h2>
[Nome e Cognome del cliente]

[info@madproduction.it]
<h2>Tipologie di Dati raccolti</h2>
Fra i Dati Personali raccolti da questa Applicazione, in modo autonomo o tramite terze parti, ci sono: Cookie e Dati di Utilizzo.

Altri Dati Personali raccolti potrebbero essere indicati in altre sezioni di questa privacy policy o mediante testi informativi visualizzati contestualmente alla raccolta dei Dati stessi.

I Dati Personali possono essere inseriti volontariamente dall’Utente, oppure raccolti in modo automatico durante l’uso di questa Applicazione.

L’eventuale utilizzo di Cookie – o di altri strumenti di tracciamento – da parte di questa Applicazione o dei titolari dei servizi terzi utilizzati da questa Applicazione, ove non diversamente precisato, ha la finalità di identificare l’Utente e registrare le relative preferenze per finalità strettamente legate all’erogazione del servizio richiesto dall’Utente.

Il mancato conferimento da parte dell’Utente di alcuni Dati Personali potrebbe impedire a questa Applicazione di erogare i propri servizi.

L’Utente si assume la responsabilità dei Dati Personali di terzi pubblicati o condivisi mediante questa Applicazione e garantisce di avere il diritto di comunicarli o diffonderli, liberando il Titolare da qualsiasi responsabilità verso terzi.
<h1>Modalità e luogo del trattamento dei Dati raccolti</h1>
<h2>Modalità di trattamento</h2>
Il Titolare tratta i Dati Personali degli Utenti adottando le opportune misure di sicurezza volte ad impedire l’accesso, la divulgazione, la modifica o la distruzione non autorizzate dei Dati Personali.

Il trattamento viene effettuato mediante strumenti informatici e/o telematici, con modalità organizzative e con logiche strettamente correlate alle finalità indicate. Oltre al Titolare, in alcuni casi, potrebbero avere accesso ai Dati categorie di incaricati coinvolti nell’organizzazione del sito (personale amministrativo, commerciale, marketing, legali, amministratori di sistema) ovvero soggetti esterni (come fornitori di servizi tecnici terzi, corrieri postali, hosting provider, società informatiche, agenzie di comunicazione) nominati anche, se necessario, Responsabili del Trattamento da parte del Titolare. L’elenco aggiornato dei Responsabili potrà sempre essere richiesto al Titolare del Trattamento.
<h3>Luogo</h3>
I Dati sono trattati presso le sedi operative del Titolare ed in ogni altro luogo in cui le parti coinvolte nel trattamento siano localizzate. Per ulteriori informazioni, contatta il Titolare.
<h3>Tempi</h3>
I Dati sono trattati per il tempo necessario allo svolgimento del servizio richiesto dall’Utente, o richiesto dalle finalità descritte in questo documento, e l’Utente può sempre chiedere l’interruzione del Trattamento o la cancellazione dei Dati.

Finalità del Trattamento dei Dati raccolti

I Dati dell’Utente sono raccolti per consentire al Titolare di fornire i propri servizi, così come per le seguenti finalità: Statistica.

Le tipologie di Dati Personali utilizzati per ciascuna finalità sono indicati nelle sezioni specifiche di questo documento.
<h2>Dettagli sul trattamento dei Dati Personali</h2>
I Dati Personali sono raccolti per le seguenti finalità ed utilizzando i seguenti servizi:
<h3>Statistica</h3>
Ulteriori informazioni sul trattamento
<h3>Difesa in giudizio</h3>
I Dati Personali dell’Utente possono essere utilizzati per la difesa da parte del Titolare in giudizio o nelle fasi propedeutiche alla sua eventuale instaurazione, da abusi nell’utilizzo della stessa o dei servizi connessi da parte dell’Utente.

L’Utente dichiara di essere consapevole che al Titolare potrebbe essere richiesto di rivelare i Dati su richiesta delle pubbliche autorità.
<h1>Informative specifiche</h1>
Su richiesta dell’Utente, in aggiunta alle informazioni contenute in questa privacy policy, questa Applicazione potrebbe fornire all’Utente delle informative aggiuntive e contestuali riguardanti servizi specifici, o la raccolta ed il trattamento di Dati Personali.
<h2>Log di sistema e manutenzione</h2>
Per necessità legate al funzionamento ed alla manutenzione, questa Applicazione e gli eventuali servizi terzi da essa utilizzati potrebbero raccogliere Log di sistema, ossia file che registrano le interazioni e che possono contenere anche Dati Personali, quali l’indirizzo IP Utente.
<h2>Informazioni non contenute in questa policy</h2>
Maggiori informazioni in relazione al trattamento dei Dati Personali potranno essere richieste in qualsiasi momento al Titolare del Trattamento utilizzando le informazioni di contatto.
<h2>Esercizio dei diritti da parte degli Utenti</h2>
I soggetti cui si riferiscono i Dati Personali hanno il diritto in qualunque momento di ottenere la conferma dell’esistenza o meno degli stessi presso il Titolare del Trattamento, di conoscerne il contenuto e l’origine, di verificarne l’esattezza o chiederne l’integrazione, la cancellazione, l’aggiornamento, la rettifica, la trasformazione in forma anonima o il blocco dei Dati Personali trattati in violazione di legge, nonché di opporsi in ogni caso, per motivi legittimi, al loro trattamento. Le richieste vanno rivolte al Titolare del Trattamento.

Questa Applicazione non supporta le richieste “Do Not Track”.

Per conoscere se gli eventuali servizi di terze parti utilizzati le supportano, l’Utente è invitato a consultare le rispettive privacy policy.
<h1>Modifiche a questa privacy policy</h1>
Il Titolare del Trattamento si riserva il diritto di apportare modifiche alla presente privacy policy in qualunque momento dandone pubblicità agli Utenti su questa pagina. Si prega dunque di consultare spesso questa pagina, prendendo come riferimento la data di ultima modifica indicata in fondo. Nel caso di mancata accettazione delle modifiche apportate alla presente privacy policy, l’Utente è tenuto a cessare l’utilizzo di questa Applicazione e può richiedere al Titolare del Trattamento di rimuovere i propri Dati Personali. Salvo quanto diversamente specificato, la precedente privacy policy continuerà ad applicarsi ai Dati Personali sino a quel momento raccolti.
<h1>Informazioni su questa privacy policy</h1>
Il Titolare del Trattamento dei Dati è responsabile per questa privacy policy.';
        } else {
            $content = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consequat, orci ac laoreet cursus, dolor sem luctus lorem, eget consequat magna felis a magna. Aliquam scelerisque condimentum ante, eget facilisis tortor lobortis in. In interdum venenatis justo eget consequat. Morbi commodo rhoncus mi nec pharetra. Aliquam erat volutpat. Mauris non lorem eu dolor hendrerit dapibus. Mauris mollis nisl quis sapien posuere consectetur. Nullam in sapien at nisi ornare bibendum at ut lectus. Pellentesque ut magna mauris. Nam viverra suscipit ligula, sed accumsan enim placerat nec. Cras vitae metus vel dolor ultrices sagittis. Duis venenatis augue sed risus laoreet congue ac ac leo. Donec fermentum accumsan libero sit amet iaculis. Duis tristique dictum enim, ac fringilla risus bibendum in. Nunc ornare, quam sit amet ultricies gravida, tortor mi malesuada urna, quis commodo dui nibh in lacus. Nunc vel tortor mi. Pellentesque vel urna a arcu adipiscing imperdiet vitae sit amet neque. Integer eu lectus et nunc dictum sagittis. Curabitur commodo vulputate fringilla. Sed eleifend, arcu convallis adipiscing congue, dui turpis commodo magna, et vehicula sapien turpis sit amet nisi.';
        }
      $add_default_pages = array(
        'post_title' => $new_page_title,
        'post_content' => $content,
        'post_status' => 'publish',
        'post_type' => 'page'
      );

      wp_insert_post($add_default_pages);
    }

    $home = get_page_by_title(__('Home', 'mad'));
    update_option('show_on_front', 'page');
    update_option('page_on_front', $home->ID);

    $home_menu_order = array(
      'ID' => $home->ID,
      'menu_order' => -1
    );
    wp_update_post($home_menu_order);
  }

  if ($mad_theme_activation_options['change_permalink_structure'] === 'true') {
    $mad_theme_activation_options['change_permalink_structure'] = false;

    if (get_option('permalink_structure') !== '/%postname%/') {
      global $wp_rewrite;
      $wp_rewrite->set_permalink_structure('/%postname%/');
      flush_rewrite_rules();
    }
  }

  if ($mad_theme_activation_options['create_navigation_menus'] === 'true') {
    $mad_theme_activation_options['create_navigation_menus'] = false;

    $roots_nav_theme_mod = false;

    $primary_nav = wp_get_nav_menu_object(__('Primary Navigation', 'mad'));

    if (!$primary_nav) {
      $primary_nav_id = wp_create_nav_menu(__('Primary Navigation', 'mad'), array('slug' => 'primary_navigation'));
      $roots_nav_theme_mod['primary_navigation'] = $primary_nav_id;
    } else {
      $roots_nav_theme_mod['primary_navigation'] = $primary_nav->term_id;
    }

    if ($roots_nav_theme_mod) {
      set_theme_mod('nav_menu_locations', $roots_nav_theme_mod);
    }
  }

  if ($mad_theme_activation_options['add_pages_to_primary_navigation'] === 'true') {
    $mad_theme_activation_options['add_pages_to_primary_navigation'] = false;

    $primary_nav = wp_get_nav_menu_object(__('Primary Navigation', 'mad'));
    $primary_nav_term_id = (int) $primary_nav->term_id;
    $menu_items= wp_get_nav_menu_items($primary_nav_term_id);

    if (!$menu_items || empty($menu_items)) {
      $pages = get_pages();
      foreach($pages as $page) {
        $item = array(
          'menu-item-object-id' => $page->ID,
          'menu-item-object' => 'page',
          'menu-item-type' => 'post_type',
          'menu-item-status' => 'publish'
        );
        wp_update_nav_menu_item($primary_nav_term_id, 0, $item);
      }
    }
  }

  update_option('mad_theme_activation_options', $mad_theme_activation_options);
  add_option( 'devmode', 'ON', '', 'yes' );
  add_option( 'minify', 'OFF', '', 'yes' );
  update_option( 'minify', 'OFF', '', 'yes' );
}
add_action('admin_init','mad_theme_activation_action');

function mad_deactivation() {
  delete_option('mad_theme_activation_options');
}
add_action('switch_theme', 'mad_deactivation');