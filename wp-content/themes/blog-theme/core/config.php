<?php
/**
 * Configuration values
 */
$ID_ga = get_option('ga_seo');
$ID_tgm = get_option('ga_tgm');
$ID_hjar = get_option('ga_hjar');
$ID_api_maps = get_option('googlemaps_key');
$font_awesome = get_option('font_awesome');
$animate_css = get_option('animate_css');
$hover_css = get_option('hover_css');

//Varie
add_theme_support('jquery-cdn');  // Enable to load jQuery from the Google CDN
//Google Analytics et similia
define('GOOGLE_TAG_MANAGER_ID', $ID_tgm); // Inserisci il codice del tag manager - se lo userai
define('GOOGLE_ANALYTICS_ID', $ID_ga); // UA-XXXXX-Y (Note: Universal Analytics only, not Classic Analytics)
define('HOTJAR_ANALYTICS_ID', $ID_hjar); // Inserisci il codice hjid di Hotjar
//Cookies popup
define('PRIVACY_MESSAGE', 'Navigando sul nostro sito accetti la privacy policy.');
define('PRIVACY_CLOSE', 'Chiudi e accetta');
define('PRIVACY_TITLE', 'La politica sulla privacy');
define('PRIVACY_URL', '/privacy-policy');
//Google maps
define('MAP_API_KEY', $ID_api_maps);


//// !!! ATTENZIONE LE LIBRERIE QUI SOTTO SONO ATTIVABILI DAL BACKEND DI WORDPRESS !!! ////
$GLOBALS['assets_options'] = array(
    'FONTAWESOME'  => $font_awesome,    // http://fontawesome.io/icons/
    'ANIMATECSS'   => $animate_css,    // https://daneden.github.io/animate.css/
    'HOVERCSS'     => $hover_css,    // http://ianlunn.github.io/Hover/
);
//// !!! ATTENZIONE LE LIBRERIE QUI SOPRA SONO ATTIVABILI DAL BACKEND DI WORDPRESS !!! ////

$GLOBALS['CSS'] = array(
    'Bootstrap'   => 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css',
    'Boostrap-engine'=> get_template_directory_uri().'/assets/includes/css/engine-bt4.css',
    'Blog Style'=> get_template_directory_uri().'/assets/includes/css/style.css',
);

$GLOBALS['JS'] = array(
    'jQuery' => 'https://code.jquery.com/jquery-3.3.1.min.js',
    'Boostrap-min' => 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js',
    'Boostrap-th' => 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js',
    'Boostrap-engine'=> get_template_directory_uri().'/assets/includes/js/engine-bt4.js',
    //'Match Height' => get_template_directory_uri().'/assets/includes/js/matchHeight.js',
    'Images Loaded'=> get_template_directory_uri().'/assets/includes/js/imagesloaded.min.js',
    // 'Gmaps' => 'https://maps.googleapis.com/maps/api/js?v=3&key='.MAP_API_KEY,
    // 'Maps' => get_template_directory_uri().'/assets/includes/js/map.js'
);