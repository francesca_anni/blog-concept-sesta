<?php
// Imposto gli script che mi interessano come async & defer
// in particolare google mappe.
function make_script_async( $tag, $handle, $src )
{
    if ( 'googlemaps' != $handle ) {
        return $tag;
    }

    return str_replace( '<script', '<script async defer', $tag );
}
add_filter( 'script_loader_tag', 'make_script_async', 10, 3 );


  /**
  * CARICO I CSS E I JS
  */
  function mad_scripts() {  
      
  // Carico la lista di CSS     
  foreach ($GLOBALS['CSS'] as $nome => $percorso) {
      wp_enqueue_style(  $nome, $percorso );
  }
  
  // Carico il CSS custom
  wp_enqueue_style(  'default', get_template_directory_uri() . '/assets/custom.css' );

  // jQuery is loaded using the same method from HTML5 Boilerplate:
  // Grab Google CDN's latest jQuery with a protocol relative URL; fallback to CDNJS if offline
  // It's kept in the header instead of footer to avoid conflicts with plugins.
  if (!is_admin() && current_theme_supports('jquery-cdn')) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://code.jquery.com/jquery-1.11.3.min.js', array(), null, false);
    add_filter('script_loader_src', 'mad_jquery_local_fallback', 10, 2);
  }
  
  wp_enqueue_script('jquery');

  // Gestisco i JS per i commenti
  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  // Carico la lista di JS     
  foreach ($GLOBALS['JS'] as $nome => $percorso) {
      wp_enqueue_script(  $nome, $percorso );
  }
  
  // Carico tutti il JS
  wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/custom.js', array(), '1.0.0', true );

  // Carico i JS e i CSS aggiuntivi in base alle opzioni
  if ($GLOBALS['assets_options']['ANIMATECSS']) {
  wp_enqueue_style(  'animate', get_template_directory_uri() . '/assets/css/animate.css' );
  }

  if ($GLOBALS['assets_options']['HOVERCSS']) {
  wp_enqueue_style(  'hover', get_template_directory_uri() . '/assets/css/hover.css' );
  }

  if ($GLOBALS['assets_options']['FONTAWESOME']) {
  wp_enqueue_script(  'hover', 'https://use.fontawesome.com/cb61676ff4.js' );
  }
  
  // Carico la compatibilità con IE9 solo su IE9
  wp_enqueue_script( 'ie9', get_template_directory_uri() . '/assets/includes/js/ie9.js', array(), '3.7.2', false );
  add_filter( 'script_loader_tag', function( $tag, $handle ) {
    if ( $handle === 'ie9' ) {
        $tag = "<!--[if lt IE 9]>$tag<![endif]-->";
    }
    return $tag;
  }, 10, 2 );

  }

add_action( 'wp_enqueue_scripts', 'mad_scripts', 100 );


// http://wordpress.stackexchange.com/a/12450
// Gestisco un sostituto per JQuery nel caso in cui il primo sia Offline
function mad_jquery_local_fallback($src, $handle = null) {
  static $add_jquery_fallback = false;

  if ($add_jquery_fallback) {
    echo '<script>window.jQuery || document.write(\'<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"><\/script>\')</script>' . "\n";
    $add_jquery_fallback = false;
  }

  if ($handle === 'jquery') {
    $add_jquery_fallback = true;
  }

  return $src;
}
add_action('wp_head', 'mad_jquery_local_fallback');

/**
 * Analytics script
 */

function mad_google_analytics() { ?>
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo GOOGLE_ANALYTICS_ID ?>"></script>
    <script>
       window.dataLayer = window.dataLayer || [];
       function gtag(){dataLayer.push(arguments);}
       gtag('js', new Date());
       gtag('config', '<?php echo GOOGLE_ANALYTICS_ID ?>');
    </script>

<?php }
if (GOOGLE_ANALYTICS_ID) {
  add_action('wp_footer', 'mad_google_analytics', 20);
}

function mad_hotjar_analytics() { ?>

<!-- Hotjar Tracking Code -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:<?php echo HOTJAR_ANALYTICS_ID; ?>,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<?php } 
if (HOTJAR_ANALYTICS_ID) {
  add_action('wp_footer', 'mad_google_analytics', 20);
}

?>