<?php

/**
 * https://codex.wordpress.org/Using_Password_Protection
 */
function protected_area_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form col-12" method="post">
    <h2>' . __( "Area Download" ) . '</h2>
    <p>' . __( "Per accedere ai download dedicati inserisci la password qui sotto." ) . '</p>
    <div class="form-group form-group-lg">
    <input class="form-control" placeholder="' . __( "Password" ) . '" name="post_password" id="' . $label . '" type="password" size="20" /></label>
    </div>
    <input class="btn btn-primary btn-lg btn-block" type="submit" name="Submit" value="' . esc_attr__( "Accedi" ) . '" />
    </form>
    ';
    return $o;
}

add_filter( 'the_password_form', 'protected_area_form' );