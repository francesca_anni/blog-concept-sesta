<?php
if ( function_exists( 'yoast_breadcrumb' ) ) {
    $yoast = $context['breadcrumbs'] = yoast_breadcrumb('<span class="breadcrumbs">','</span>', false );
}

$catalogo_collezioni = get_field('catalogo', 256);
$scheda_tecnica_collezioni = get_field('scheda_tecnica', 256);
$rivestimenti_collezioni = get_field('rivestimenti', 256);
$listino_collezioni = get_field('listino_prezzi', 256);
$immagini_collezioni = get_field('archivio_immagini', 256);
$cad_collezioni = get_field('disegni_cad', 256);

$GLOBALS['context'] = array(
    'content'   		=> get_the_content(),
    'title'     		=> get_the_title(),
    'post_class'		=> get_post_class()[0],
    'time'				=> get_the_time('c'),
    'date'				=> get_the_date(),
    'author_url'		=> get_author_posts_url(get_the_author_meta('ID')),
    'author'			=> get_the_author(),
    'breadcrumb_yoast'    => $yoast,

    'template_root' => get_template_directory_uri(),
    'site_url' => get_site_url(),
    'area_riservata' => 'www.sesta.it/area-riservata.html',
    
    // DEXANET THEME OPTIONS
    'dex_url_facebook'		=> get_option('facebook_url'),
    'dex_url_twitter'		=> get_option('twitter_url'),
    'dex_url_instagram'		=> get_option('instagram_url'),
    'dex_url_googleplus'	=> get_option('googleplus_url'),
    'dex_url_pinterest'	=> get_option('pinterest_url'),

	'dex_img_logo'				=> get_option('header_logo'),
	'dex_img_logo_secondary'	=> get_option('header_logo_secondary'),
	'dex_alt_logo'				=> get_option('header_logo_alt'),

	'dex_rs'				=> get_option('contact_ragione_sociale'),
	'dex_indirizzo'			=> get_option('contact_indirizzo'),
	'dex_telefono'			=> get_option('contact_telefono'),
    'dex_fax'			=> get_option('contact_fax'),
	'dex_mail'				=> get_option('contact_mail'),
    'dex_vat'				=> get_option('contact_vat_number'),
    'dex_orari'				=> get_option('contact_orari'),

    // TRADUZIONI
    'new_tit' => get_field('new_tit', 'option'),
    'area_riservata_tit' => get_field('area_riservata_tit', 'option'),
    'link_sesta' => get_field('link_sesta', 'option'),
    'link_area_riservata' => get_field('link_area_riservata', 'option'),
    'scheda_prodotto_tit' => get_field('scheda_prodotto_tit', 'option'),
    'disegni_tecnici_tit' => get_field('disegni_tecnici_tit', 'option'),
    'finiture_tit' => get_field('finiture_tit', 'option'),
    'download_tit' => get_field('download_tit', 'option'),
    'catalogo_tit' => get_field('catalogo_tit', 'option'),
    'scheda_tecnica_tit' => get_field('scheda_tecnica_tit', 'option'),
    'rivestimenti_tit' => get_field('rivestimenti_tit', 'option'),
    'listino_prezzi_tit' => get_field('listino_prezzi_tit', 'option'),
    'archivio_immagini_tit' => get_field('archivio_immagini_tit', 'option'),
    'disegni_cad_tit' => get_field('disegni_cad_tit', 'option'),
    'vai_a_collezioni' => get_field('vai_a_collezioni', 'option'),
    'link_collezioni' => get_field('link_collezioni', 'option'),
    'pagina_area_riservata' => get_field('pagina_area_riservata', 'option'),
    'frase_operatore_di_settore' => get_field('frase_operatore_di_settore', 'option'),
    'richiedi_accesso' => get_field('richiedi_accesso', 'option'),


    // CONTACT FORM
    'contact_form'  => do_shortcode('[contact-form-7 id="12" title="Contatti"]'),

    // HOME
    'slide_home' => get_field('slide'),
    'img_slide' => get_sub_field('img_slide'),

    'pagina_collezioni' => get_field('pagina_collezioni'),
    'homepage'  => get_field('homepage'),
    'row_collezioni'    => get_field('row_collezioni'),
    'row_combinazioni'    => get_field('row_combinazioni'),
    'row_concept'    => get_field('row_concept'),
    'row_designer'    => get_field('row_designer'),
    'row_cta'    => get_field('row_cta'),
    'row_mosaico'    => get_field('row_mosaico'),

    // ARCHIVE COLLEZIONI
    'img_collezioni' => get_field('img_collezioni'),
    'testo_collezioni'  => get_field('testo_collezioni'),

    // SINGLE COLLEZIONI
    'categoria_prodotti'  => get_field('categoria_prodotti'),
    'categoria_prodotti2'  => get_field('categoria_prodotti2'),
    'modelli_metal'   => get_field('modelli_metal'),
    'modelli_wood'   => get_field('modelli_wood'),
    'img_modello'  => get_sub_field('img_modello'),
    'nome_modello'  => get_sub_field('nome_modello'),
    'new'   =>  get_sub_field('new'),
    'scheda_prodotto'   => get_field('scheda_prodotto'),

    /* DISEGNI TECNICI */
    'disegni_tecnici'   => get_field('disegni_tecnici'),
    'titolo_disegno'  => get_sub_field('titolo_disegno'),
    'img_disegno'  => get_sub_field('img_disegno'),

    /* FINITURE */
    'gruppo_finitura'   => get_field('gruppo_finitura'),
    'nome_gruppo_finitura'   => get_sub_field('nome_gruppo_finitura'),
    'desc_gruppo_finitura'   => get_sub_field('desc_gruppo_finitura'),
    'elenco_finiture'   => get_sub_field('elenco_finiture'),
    'dettagli_finiture'   => get_sub_field('dettagli_finiture'),
    'img_colore'   => get_sub_field('img_colore'),
    'codice_colore'   => get_sub_field('codice_colore'),
    'nome_colore'   => get_sub_field('nome_colore'),

    /* DOWNLOAD */
    'catalogo'  => get_field('catalogo'),
    'scheda_tecnica'  => get_field('scheda_tecnica'),
    'rivestimenti'  => get_field('rivestimenti'),
    'listino_prezzi'  => get_field('listino_prezzi'),
    'archivio_immagini'  => get_field('archivio_immagini'),
    'disegni_cad'  => get_field('disegni_cad'),

    /* DOWNLOAD DELLA PAGINA COLLEZIONI */
    'catalogo_collezioni' => $catalogo_collezioni,
    'scheda_tecnica_collezioni' =>$scheda_tecnica_collezioni,
    'rivestimenti_collezioni' => $rivestimenti_collezioni,
    'listino_collezioni' => $listino_collezioni,
    'immagini_collezioni' => $immagini_collezioni,
    'cad_collezioni' => $cad_collezioni,

  );
?>