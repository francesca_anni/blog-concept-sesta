<?php get_template_part('templates/head'); ?>
<?php if ( GOOGLE_TAG_MANAGER_ID ) : ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
   })(window,document,'script','dataLayer','<?php echo GOOGLE_TAG_MANAGER_ID; ?>');
</script>
<!-- End Google Tag Manager -->
<?php endif; ?>

<?php if ( GOOGLE_TAG_MANAGER_ID ) : ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo GOOGLE_TAG_MANAGER_ID; ?>"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php endif; ?>   
<?php $layout = get_option('theme_layout'); ?>
<body <?php body_class(); ?>>

  <?php
    do_action('get_header');
    get_template_part('templates/header');
    get_template_part('templates/menu');
  ?>

  <?php include mad_template_path(); ?>

  <?php get_template_part('templates/footer'); ?>
  <?php wp_footer(); ?>


  <div class="modal fade" id="myModal">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                  <?php
                  $blog_id = get_current_blog_id();
                  if ( 1 == $blog_id ) { ?>
                      <h2 class="tit-section tit-second orange">Accedi all'area riservata</h2>
                  <?php } if ( 2 == $blog_id ) { ?>
                      <h2 class="tit-section tit-second orange">Reserved Area</h2>
                  <?php } if ( 3 == $blog_id ) { ?>
                      <h2 class="tit-section tit-second orange">Accéder à la zone réservée</h2>
                  <?php } ?>
                  <?php wp_login_form(); ?>
                  <?php
                  $blog_id = get_current_blog_id();
                  if ( 1 == $blog_id ) { ?>
                      <p>Non hai un account, <a href="/user-login/" target="_self" title="Registrati ora all'area riservata di Blog"><b>registrati ora</b></a></p>
                  <?php } if ( 2 == $blog_id ) { ?>
                      <p>Do not have an account, <a href="/user-login/" target="_self" title="Registrati ora all'area riservata di Blog"><b>register now</b></a></p>
                  <?php } if ( 3 == $blog_id ) { ?>
                      <p>Vous n'avez pas de compte, <a href="/user-login/" target="_self" title="Registrati ora all'area riservata di Blog"><b>inscrivez-vous</b></a></p>
                  <?php } ?>
              </div>
          </div>
      </div>
  </div>
  <script>
      $( document ).ready(function() {
          $("#rememberme").after("<span></span>");
      });
  </script>
  <script>
      $('#myModal').modal('hide')
  </script>
  <script>
      $( document ).ready(function() {
         $("#menu-collezioni").insertAfter(".has-mega-menu a");
      });

      function isTouchDevice(){
          return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
      }
      if(isTouchDevice()===true) {
          $(".has-mega-menu a").addClass("nav-link dropdown-toggle").attr("data-toggle", "dropdown");
      }
      else {
          $(".link-collezioni").hide();
      }


      $('.has-mega-menu').on('click',function(){
          // handle click event, put money in my bank account
      }).on('mouseenter',function(){
          $(".has-mega-menu").addClass("show");
          $("#menu-collezioni").addClass("show");// only on enter here
          // handle hover mouse enter of hover event, put money in my bank account
      }).on('mouseleave',function(){
          $(".has-mega-menu").removeClass("show");
          $("#menu-collezioni").removeClass("show");
          // handle mouse leave event of hover, put money in my bank account
      })

  </script>
  <script>
      // Equal height col
      $(window).ready(function () {
          updateContainer();
          $(window).resize(function() {
              updateContainer();
          });
      });
      function updateContainer() {

          $('.two-blocks').each(function() {
              $('.big-quad').matchHeight({
                  byRow:false
              });
              $('.med-quad').matchHeight({
                  byRow:false
              });
          });

          $('.mosaic').each(function() {
              $('.quad').matchHeight({
                  byRow:false
              });
          });
      }

      // Hover delle immagini
      $('.metal').hover(function(){
          $('.metal-img').toggleClass('hidden');
          $('.hover-image.visible').toggleClass('hidden');
      });
      $('.wood').hover(function(){
          $('.wood-img').toggleClass('hidden');
          $('.hover-image.visible').toggleClass('hidden');
      });
      $('.txt-hover-link').hover(function(){
          $(this).parent().siblings('.txt-hover').toggleClass('hidden');
      });
  </script>
  <script>
      $('.lingua-link a.current_language').prependTo('.lingua-link').removeAttr("href");
  </script>
</body>
</html>
