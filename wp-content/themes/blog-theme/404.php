<?php get_template_part('templates/page', 'header'); ?>
<div class="nb-error">
      <div class="error-code">404</div>
      <h3 class="font-bold">We couldn't find the page..</h3>
      <div class="error-desc">
          Sorry, but the page you are looking for was either not found or does not exist. <br/>
          Try refreshing the page or click the button below to go back to the Homepage.
            <?php get_search_form(); ?>
        <div class="text-center">
       </div>
      </div>
  </div>
</div>