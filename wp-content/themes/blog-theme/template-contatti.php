<?php
/*
Template Name: Template Contatti
*/
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/contatti/slide'); ?>
    <?php get_template_part('templates/contatti/title'); ?>
    <?php get_template_part('templates/contatti/contatti'); ?>
<?php endwhile; ?>
