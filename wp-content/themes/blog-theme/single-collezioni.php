<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/collezioni/slide'); ?>
    <?php get_template_part('templates/collezioni/title'); ?>
    <?php get_template_part('templates/collezioni/loop'); ?>
    <?php get_template_part('templates/collezioni/details'); ?>
    <?php get_template_part('templates/collezioni/download'); ?>
<?php endwhile; ?>
