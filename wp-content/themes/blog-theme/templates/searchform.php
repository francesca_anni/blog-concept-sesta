<form class="text-center" role="search" method="get" action="<?php echo esc_url(home_url('/')); ?>">
	<div class="form-group">
	  <label class="sr-only"><?php _e('Search for:', 'mad'); ?></label>
	  <div class="input-group">
	    <input type="search" value="<?php echo get_search_query(); ?>" name="s" class="search-field form-control" placeholder="<?php _e('Search', 'roots'); ?> <?php bloginfo('name'); ?>" required>
		</div>
	</div>
	<button type="submit" class="btn btn-primary"><?php _e('Search', 'mad'); ?></button>
</form>