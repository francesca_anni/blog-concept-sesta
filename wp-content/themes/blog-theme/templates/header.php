<?php 
    $layout_menu = get_option('menu_layout');
    $logo_brand = get_option('header_logo');
    $logo_brand_alt = get_option('header_logo_alt');
    $menu_position = get_option('menu_position_settings');
?>

<!-- MENU -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" title="Blog">
            <?php
            if ($logo_brand == ""){
                bloginfo('name');
            } else {
                echo '<img class="logo-nav" src="'.$logo_brand.'" alt="'.$logo_brand_alt.'">';
            }
            ?>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <?php
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(array('theme_location' => 'primary_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'navbar-nav ml-auto'));
            endif;
            ?>
        </div>
    </div>
</nav>

<section class="pre-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="lingua-link">
                    <?php if ( function_exists( 'the_msls' ) ) the_msls(); ?>
                </div>
                <ul class="login-nav">
                    <li class="facebook-link">
                        <a href="<?php echo get_option('facebook_url') ?>" target="_blank" title="Facebook Blog Sesta">
                            <img src="<?php echo get_template_directory_uri() ?>/img/utility/facebook.svg" alt="Facebook Blog Sesta" title="Facebook Blog Sesta" />
                        </a>
                    </li>
                    <li class="instagram-link">
                        <a href="<?php echo get_option('instagram_url') ?>" target="_blank" title="Instagram Blog Sesta">
                            <img src="<?php echo get_template_directory_uri() ?>/img/utility/instagram.svg" alt="Instagram Blog Sesta" title="Instagram Blog Sesta" />
                        </a>
                    </li>
                    <li class="pinterest-link">
                        <a href="<?php echo get_option('pinterest_url') ?>" target="_blank" title="Pinterest Blog Sesta">
                            <img src="<?php echo get_template_directory_uri() ?>/img/utility/pinterest.svg" alt="Pinterest Blog Sesta" title="Pinterest Blog Sesta" />
                        </a>
                    </li>
                    <li class="pinterest-link">
                        <a href="https://pcon-planner.com/it/" target="_blank" title="Pinterest Blog Sesta">
                            <img style="width: 37px;" src="<?php echo get_template_directory_uri() ?>/img/utility/pcon2.png" alt="Pcon Blog Sesta" title="Pcon Blog Sesta" />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
