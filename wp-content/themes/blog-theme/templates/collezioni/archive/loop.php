<?php

$context = Timber::get_context();

$args = array(
    'post_type' => 'collezioni', // Nome del custom post
    'posts_per_page' => 6,
);

$posts = Timber::get_posts( $args );
$context['posts'] = $posts;
$context['global'] = $GLOBALS['context'];
Timber::render( 'collezioni/archive/loop.twig.php', $context, 600 );
