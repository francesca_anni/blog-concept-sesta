<?php
if( current_user_can('aziende') || current_user_can('administrator') ) {
    Timber::render('collezioni/download.twig.php', $GLOBALS['context'], 600);
} elseif ( current_user_can('designer')) {
    Timber::render('collezioni/download-cad.twig.php', $GLOBALS['context'], 600);
} else {
    Timber::render('collezioni/download-no.twig.php', $GLOBALS['context'], 600);
}
?>
