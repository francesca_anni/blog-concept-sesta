<?php 
    $layout_menu = get_option('footer_layout'); 
    $footer_text = get_option('footer_text'); 
?>
<section class="bg-dark-sesta pre-footer">
    <div class="container">
        <div class="row">
            <div class="col logo-sesta">
                <a href="<?php echo get_field('link_sesta', 'option'); ?>" target="_blank" title="Sesta"><img src="<?php echo get_template_directory_uri() ?>/img/sesta-logo-white.svg" alt="Sesta" title="Sesta"></a>
            </div>
            <div class="col text-center">
                <a class="to-top" href="#top" title="Blog"><img src="<?php echo get_template_directory_uri() ?>/img/utility/top.svg"></a>
            </div>
            <div class="col text-right blog-sesta">
                <a href="<?php echo get_option('facebook_url') ?>" target="_blank" title="Sesta"><img src="<?php echo get_template_directory_uri() ?>/img/utility/facebook-white.svg" alt="Blog - Sesta" ></a>
                <a href="<?php echo get_option('instagram_url') ?>" target="_blank" title="Sesta"><img src="<?php echo get_template_directory_uri() ?>/img/utility/instagram-white.svg" alt="Blog - Sesta" ></a>
                <a href="<?php echo get_option('pinterest_url') ?>" target="_blank" title="Sesta"><img src="<?php echo get_template_directory_uri() ?>/img/utility/pinterest-white.svg" alt="Blog - Sesta" ></a>
                <a href="https://pcon-planner.com/it/" target="_blank" title="Sesta Pcon"><img style="width: 45px;" src="<?php echo get_template_directory_uri() ?>/img/utility/pcon-white.png" alt="Blog - Sesta" ></a>
            </div>
        </div>
    </div>
</section>
<footer class="bg-dark-sesta" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-8 col-lg-2">
                <?php Timber::render('info-footer.twig.php', $GLOBALS['context']); ?>
            </div>
            <div class="col-12 col-lg-8 flex-last text-center">
                <?php
                if (has_nav_menu('footer_menu')) :
                    wp_nav_menu(array('theme_location' => 'footer_menu', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'footer-menu'));
                endif;
                ?>
                <small><?php echo $footer_text; ?></small>
                <?php
                if (has_nav_menu('mini_menu')) :
                    wp_nav_menu(array('theme_location' => 'mini_menu', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'mini-menu'));
                endif;
                ?>
            </div>
            <div class="col-4 col-lg-2 certificazioni text-right">
                <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/utility/iso.svg" />
            </div>
        </div>
    </div>
</footer>



