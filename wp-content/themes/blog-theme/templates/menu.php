<?php

$context = Timber::get_context();

$args = array(
    'post_type' => 'collezioni',
    'posts_per_page' => 6,
);

$posts = Timber::get_posts( $args );
$context['posts'] = $posts;

Timber::render( 'menu.twig.php', $context );

?>
<div class="col-12 text-right link-collezioni">
    <a style="color: #e65214;" href="<?php echo get_field('link_collezioni', 'option'); ?>"><?php echo get_field('vai_a_collezioni', 'option'); ?><br></a>
</div>
</div>
</div>
</div>
