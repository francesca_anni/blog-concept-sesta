<div id="menu-collezioni" class="dropdown-menu text-center">
    <div class="px-0 container-fluid">
        <div class="row">
            {% for post in posts %}
            <div class="col-6 col-lg-2">
                <a class="dropdown-item" href="{{ post.link }}" title="{{ post._yoast_wpseo_title }}"><span>{{ post.title }}</span><br>
                    <img class="img-fluid" src="{{TimberImage(post.get_field('img_menu')).src}}" alt="{{ post.title }}" title="{{ post._yoast_wpseo_title }}"/>
                </a>
            </div>
            {% endfor %}
