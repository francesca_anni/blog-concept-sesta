<p>
    {{dex_indirizzo}}
</p>
<p>
    <a onclick="gtag('event', 'click', { 'event_category': 'Click Telefono', 'event_action': 'click' });" href="tel:{{dex_telefono}}" target="_blank" title="Chiama {{dex_telefono}}">
        T. {{dex_telefono}}
    </a><br> F. {{dex_fax}}<br>
    <a onclick="gtag('event', 'click', { 'event_category': 'Click Email', 'event_action': 'click' });" href="mailto:{{dex_mail}}" target="_blank" title="Scrivi a {{dex_mail}}">
        {{dex_mail}}
    </a><br>
    <a href="{{link_sesta}}" target="_blank" title="Visita {{link_sesta}}">
        www.sesta.it
    </a>
</p>