<section class="contatti">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <p>
                            <img class="img-fluid" src="{{dex_img_logo}}" alt="{{dex_alt_logo}}" title="{{dex_alt_logo}}" />
                        </p>
                        <br>
                    </div>
                    <div class="col-12 col-md-4">
                        <p>
                            {{dex_rs}}<br>
                            {{dex_indirizzo}}
                        </p>
                        <p>
                            <a onclick="gtag('event', 'click', { 'event_category': 'Click Telefono', 'event_action': 'click' });" href="tel:{{dex_telefono}}" target="_blank" title="Chiama {{dex_telefono}}">
                                {{dex_telefono}}
                            </a>
                            <br>
                            {{dex_fax}}<br>
                            <a onclick="gtag('event', 'click', { 'event_category': 'Click Email', 'event_action': 'click' });" href="mailto:{{dex_mail}}" target="_blank" title="Scrivi a {{dex_mail}}">
                                {{dex_mail}}
                            </a>
                        </p>
                    </div>
                    <div class="col-12 col-md-4">
                        <p>{{dex_orari}}</p>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <br>
                {{contact_form}}
            </div>
        </div>
    </div>
</section>