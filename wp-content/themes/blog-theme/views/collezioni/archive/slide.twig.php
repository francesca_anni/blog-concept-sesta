<section class="two-blocks intro-collezioni">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-12 col-lg-6">
                <img class="img-fluid immagine big-quad" src="{{ TimberImage(img_collezioni).src }}" alt="{{ TimberImage(img_collezioni).alt }}" title="{{ TimberImage(img_collezioni).alt }}">
            </div>
            <div class="col-12 col-lg-6 blocco vert-align bg-dark-sesta big-quad">
                <div class="vert-cont">
                    {{testo_collezioni}}
                </div>
            </div>
        </div>
    </div>
</section>