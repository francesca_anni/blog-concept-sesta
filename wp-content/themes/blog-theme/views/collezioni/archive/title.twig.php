<!-- CONTENT -->
<section id="start" class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col">
                {{breadcrumb_yoast}}
            </div>
        </div>
    </div>
</section>

<section class="tit-page">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h2 class="tit-section tit-catalog"><span>{{title}}</span></h2>
                <h2 class="tit-section tit-second"><span>Metal</span></h2>
            </div>
        </div>
    </div>
</section>