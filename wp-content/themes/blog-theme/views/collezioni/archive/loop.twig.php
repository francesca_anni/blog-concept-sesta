<section class="prodotti-categoria categoria-metal">
    <div class="container">
        <div class="row">
            {% for collezione in posts %}
            <div class="col-6 col-md-4 foto-modello">
                <a href="{{collezione.link}}" target="_self" title="{{ collezione._yoast_wpseo_title }}">
                    {% for metal in collezione.get_field('modelli_metal') %}
                        {% if metal.in_evidenza == true %}
                            <img class="img-fluid" src="{{ TimberImage(metal.img_modello).src }}" alt="{{ TimberImage(metal.img_modello).alt }}" title="{{ TimberImage(metal.img_modello).alt }}"/>
                        {% endif %}
                    {% endfor %}
                    <h3 class="tit-prod uppercase">{{collezione.title}}</h3>
                </a>
            </div>
            {% endfor %}
        </div>
    </div>
</section>

<section class="tit-page wood-section">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h2 class="tit-section tit-second"><span>Wood</span></h2>
            </div>
        </div>
    </div>
</section>

<section class="prodotti-categoria categoria-wood">
    <div class="container">
        <div class="row">
            {% for collezione in posts %}
            <div class="col-6 col-md-4 foto-modello {{collezione.title|sanitize}}">
                <a href="{{collezione.link}}" target="_self" title="{{ collezione._yoast_wpseo_title }}">
                    {% for wood in collezione.get_field('modelli_wood') %}
                    {% if wood.in_evidenza == true %}
                    <img class="img-fluid" src="{{ TimberImage(wood.img_modello).src }}" alt="{{ TimberImage(wood.img_modello).alt }}" title="{{ TimberImage(wood.img_modello).alt }}"/>
                    {% endif %}
                    {% endfor %}
                    <h3 class="tit-prod uppercase">{{collezione.title}}</h3>
                </a>
            </div>
            {% endfor %}
        </div>
    </div>
</section>