<section class="prodotti-categoria">
    <div class="container">
        <div class="row">
            {% for post in modelli_metal %}
            <div class="col-6 col-md-4">
                <img class="img-fluid" src="{{ TimberImage(post.img_modello).src }}" alt="{{ TimberImage(post.img_modello).alt }}" title="{{ TimberImage(post.img_modello).alt }}"/>
                <h3 class="tit-prod">{{post.nome_modello}}</h3>
                {% if post.new == 1 %} <h3 class="new">{{new_tit}}</h3> {% endif %}
            </div>
            {% endfor %}
        </div>
    </div>
</section>

{% if categoria_prodotti2 %}
<section class="tit-page wood-section">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h2 class="tit-section tit-second"><span>{{categoria_prodotti2}}</span></h2>
            </div>
        </div>
    </div>
</section>
{% endif %}

{% if modelli_wood %}
<section class="prodotti-categoria">
    <div class="container">
        <div class="row">
            {% for post2 in modelli_wood %}
            <div class="col-6 col-md-4">
                <img class="img-fluid" src="{{ TimberImage(post2.img_modello).src }}" alt="{{ TimberImage(post2.img_modello).alt }}" title="{{ TimberImage(post2.img_modello).alt }}"/>
                <h3 class="tit-prod">{{post2.nome_modello}}</h3>
                {% if post2.new == 1 %} <h3 class="new">{{new_tit}}</h3> {% endif %}
            </div>
            {% endfor %}
        </div>
    </div>
</section>
{% endif %}