    <!-- DOWNLOAD LIMITATI -->
    <section class="download">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-12 text-center">
                    <h2 class="tit-section"><span>{{download_tit}}</span></h2>
                </div>
                <div class="col-12 col-md-6 col-lg-4 text-left">
                    <a href="{% if catalogo %}{{ TimberImage(catalogo).src }}{% else %}{{ TimberImage(catalogo_collezioni).src }}{% endif %}" target="_blank" class="btn btn-sesta btn-download" title="{{catalogo_tit}}">{{catalogo_tit}}</a>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="{% if scheda_tecnica %}{{ TimberImage(scheda_tecnica).src }}{% else %}{{ TimberImage(scheda_tecnica_collezioni).src }}{% endif %}" target="_blank" class="btn btn-sesta btn-download" title="{{scheda_tecnica_tit}}">{{scheda_tecnica_tit}}</a>
                </div>
                <div class="col-12 col-md-6 col-lg-4 text-right">
                    <a href="{% if rivestimenti %}{{ TimberImage(rivestimenti).src }}{% else %}{{ TimberImage(rivestimenti_collezioni).src }}{% endif %}" target="_blank" class="btn btn-sesta btn-download" title="{{rivestimenti_tit}}">{{rivestimenti_tit}}</a>
                </div>
                <div class="col-12 col-md-6 col-lg-4 text-left disable">
                    <a data-toggle="modal" data-target="#myModal" class="btn btn-sesta btn-download" title="{{listino_prezzi_tit}}">{{listino_prezzi_tit}}</a>
                </div>
                <div class="col-12 col-md-6 col-lg-4 disable">
                    <a data-toggle="modal" data-target="#myModal" class="btn btn-sesta btn-download" title="{{archivio_immagini_tit}}">{{archivio_immagini_tit}}</a>
                </div>
                <div class="col-12 col-md-6 col-lg-4 text-right disable">
                    <a data-toggle="modal" data-target="#myModal" class="btn btn-sesta btn-download" title="{{disegni_cad_tit}}">{{disegni_cad_tit}}</a>
                </div>
            </div>
        </div>
    </section>
