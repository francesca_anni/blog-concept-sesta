{% if scheda_prodotto %}
<section class="scheda-prodotto">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="tit-section"><span>{{scheda_tecnica_tit}}</span></h2>
            </div>
            <div class="col">
                <p class="p-2-columns">
                    {{scheda_prodotto}}
                </p>
            </div>
        </div>
    </div>
</section>
{% endif %}

{% if disegni_tecnici %}
<section class="disegni-tecnici">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h2 class="tit-section"><span>{{disegni_tecnici_tit}}</span></h2>
            </div>
            <div class="col-12 col-lg-10">
                {% for post in disegni_tecnici %}
                    <p class="p-detail">{{ post.titolo_disegno }}</p>
                    <img class="img-fluid" src="{{ TimberImage(post.img_disegno).src }}" alt="{{ TimberImage(post.img_disegno).alt }}" title="{{ post.titolo_disegno }}">
                {% endfor %}
            </div>
        </div>
    </div>
</section>
{% endif %}

{% if gruppo_finitura %}
<section class="finiture">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h2 class="tit-section"><span>{{finiture_tit}}</span></h2>
            </div>
            {% for post in gruppo_finitura %}
                <div class="col-12 gruppo-finitura">
                    <div class="row">
                        <div class="col-2">
                            <p class="cat-finitura"><span class="text-uppercase">{{ post.nome_gruppo_finitura }}</span><br>{{ post.desc_desc_finitura }}</p>
                        </div>
                        <div class="col-10">
                            <div class="row">
                                {% for elenco in post.elenco_finiture %}
                                    <div class="col-4 col-md-3 col-lg-2 {% if elenco.img_colore %} {% else %} {{ elenco.codice_colore|sanitize }} {% endif %}">
                                        <div class="finitura">
                                            {% if elenco.img_colore %}
                                                <img class="img-fluid" src="{{ TimberImage(elenco.img_colore).src }}" alt="{{ TimberImage(elenco.img_colore).alt }}"/>
                                            {% endif %}
                                        </div>
                                        <p><span class="codice-colore">{{ elenco.codice_colore }}</span><br>{{ elenco.nome_colore }}</p>
                                    </div>
                                {% endfor %}

                                {% if post.dettagli_finiture %}
                                <div class="col-4 col-md-4 col-lg-4">
                                    <p>{{ post.dettagli_finiture }}</p>
                                </div>
                                {% endif %}
                            </div>
                        </div>
                    </div>
                </div>
            {% endfor %}
        </div>
    </div>
</section>
{% endif %}