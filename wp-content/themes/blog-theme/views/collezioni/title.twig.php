<section id="start" class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col">
                {{breadcrumb_yoast}}
            </div>
        </div>
    </div>
</section>

<section class="tit-page">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h2 class="tit-section tit-catalog"><span>{{title}}</span></h2>
                {% if categoria_prodotti %} <h2 class="tit-section tit-second"><span>{{categoria_prodotti}}</span></h2> {% endif %}
            </div>
        </div>
    </div>
</section>