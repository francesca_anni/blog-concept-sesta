{% for flexi in homepage %}
    {% if flexi.acf_fc_layout == "row_collezioni" %}
        <section id="start" class="two-blocks collezioni">
            <div class="container-fluid">
                <div class="row no-gutters box-hover-image">
                    <div class="col-12 col-md-6">
                        <img class="img-fluid immagine hover-image visible big-quad" src="{{flexi.img_collezioni.url}}" alt="{{flexi.img_collezioni.alt}}" title="{{flexi.img_collezioni.alt}}"/>
                        <img class="img-fluid immagine hover-image wood-img hidden big-quad" src="{{flexi.img_wood.url}}" alt="{{flexi.img_wood.alt}}" title="{{flexi.img_wood.alt}}"/>
                        <img class="img-fluid immagine hover-image metal-img hidden big-quad" src="{{flexi.img_metal.url}}" alt="{{flexi.img_metal.alt}}" title="{{flexi.img_metal.alt}}"/>
                    </div>
                    <div class="col-12 col-md-6 blocco vert-align text-center bg-dark-sesta big-quad">
                        <div class="vert-cont">
                            <h2 class="tit-sesta"><a href="{{pagina_collezioni}}" title="{{flexi.titolo}}"><span>{{flexi.titolo}}</span></a></h2>
                            <h5 class="tit-cat metal"><a href="{{pagina_collezioni}}" title="Metal Collection">Metal Collection</a></h5>
                            <h5 class="tit-cat wood"><a href="{{pagina_collezioni}}" title="Wood Collection">Wood Collection</a></h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    {% elseif flexi.acf_fc_layout == "row_combinazioni" %}
        <section class="four-blocks combinazioni">
            <div class="container-fluid">
                <div class="row no-gutters">
                    <div class="col-6 col-lg-3 blocco-comb vert-align text-center bg-dark-sesta med-quad">
                        <div class="vert-cont">
                            <h2 class="tit-sesta no-icon"><span>{{flexi.titolo}}</span></h2>
                        </div>
                    </div>
                    {% for img in flexi.griglia_img %}
                    <div class="col-6 col-lg-3">
                        <img class="img-fluid immagine-comb med-quad" src="{{img.img.url}}" alt="{{img.img.alt}}" title="{{img.img.alt}}"/>
                    </div>
                    {% endfor %}
                </div>
            </div>
        </section>
    {% elseif flexi.acf_fc_layout == "row_concept" %}
        <section class="two-blocks concept">
            <div class="container-fluid">
                <div class="row no-gutters">
                    <div class="col-12 col-md-6 blocco vert-align text-center bg-dark-sesta big-quad">
                        <div class="vert-cont">
                            <h2 class="tit-sesta txt-hover-link"><span>{{flexi.titolo}}</span></h2>
                        </div>
                        <div class="txt-hover hidden">
                            {{flexi.testo}}
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <img class="img-fluid immagine big-quad" src="{{flexi.img.url}}" alt="{{flexi.img.alt}}" title="{{flexi.img.alt}}"/>
                    </div>
                </div>
            </div>
        </section>
    {% elseif flexi.acf_fc_layout == "row_designer" %}
        <section class="two-blocks concept designer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col text-center">
                        <h2 class="tit-section pinterest">
                            <span>Follow us</span>
                            <a href="{{dex_url_facebook}}" target="_blank" title="Sesta"><img src="https://www.blogconcept.sesta.it/wp-content/themes/blog-theme/img/utility/facebook.svg" alt="Blog - Sesta" ></a>
                            <a href="{{dex_url_instagram}}" target="_blank" title="Sesta"><img src="https://www.blogconcept.sesta.it/wp-content/themes/blog-theme/img/utility/instagram.svg" alt="Blog - Sesta" ></a>
                            <a href="{{dex_url_pinterest}}" target="_blank" title="Sesta"><img src="https://www.blogconcept.sesta.it/wp-content/themes/blog-theme/img/utility/pinterest.svg" alt="Blog - Sesta" ></a>
                            <a href="https://pcon-planner.com/it/" target="_blank" title="Sesta"><img style="max-width: none; max-height: 30px;" src="https://www.blogconcept.sesta.it/wp-content/themes/blog-theme/img/utility/pcon.png" alt="Blog - Sesta" ></a>
                        </h2>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-12 col-md-6">
                        <h5 class="txt-cit">
                            {{flexi.citazione}}
                            <span class="designer">{{flexi.autore}}</span>
                        </h5>
                        <img class="img-fluid immagine big-quad" src="{{flexi.img.url}}" alt="{{flexi.img.alt}}" title="{{flexi.img.alt}}"/>
                    </div>
                    <div class="col-12 col-md-6 blocco vert-align text-center bg-dark-sesta big-quad">
                        <div class="vert-cont">
                            <h2 class="tit-sesta txt-hover-link"><span>{{flexi.titolo}}</span></h2>
                        </div>
                        <div class="txt-hover hidden">
                            <h5 class="tit-cat">{{flexi.autore}}</h5>
                            {{flexi.testo}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    {% elseif flexi.acf_fc_layout == "row_cta" %}
        <section class="bg-light-sesta cta">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 vert-align text-center bg-light">
                        <div class="vert-cont">
                            <h2 class="tit-sesta txt-hover-link">{{flexi.titolo}}</h2>
                            <h4>{{flexi.testo}}</h4>
                            <a href="{{flexi.link}}" title="{{flexi.titolo}} {{flexi.bottone}}" class="btn btn-sesta">{{flexi.bottone}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    {% elseif flexi.acf_fc_layout == "row_mosaico" %}
        <section class="four-blocks mosaic">
            <div class="container-fluid">
                <div class="row">
                    <div class="col text-center">
                        <h2 class="tit-section"><span>Mosaic</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-15 col-mosaic">
                        <div class="bg-dark-sesta vert-align text-center quad">
                            <div class="vert-cont">
                                <h2 class="tit-sesta no-icon"><span><span class="txt-light">Blog</span>{{flexi.titolo}}</span></h2>
                            </div>
                        </div>
                        <img class="img-fluid" src="{{flexi.img_ret.url}}" alt="{{flexi.img_ret.alt}}" title="{{flexi.img_ret.alt}}"/>
                    </div>
                    <div class="col-md-15 col-mosaic">
                        <img class="img-fluid quad" src="{{flexi.img_quad.url}}" alt="{{flexi.img_quad.alt}}" title="{{flexi.img_quad.alt}}"/>
                        <img class="img-fluid" src="{{flexi.img_ret2.url}}" alt="{{flexi.img_ret2.alt}}" title="{{flexi.img_ret2.alt}}"/>
                    </div>
                    <div class="col-md-15 col-mosaic">
                        <img class="img-fluid" src="{{flexi.img_ret3.url}}" alt="{{flexi.img_ret2.alt}}" title="{{flexi.img_ret2.alt}}"/>
                        <div class="bg-dark-sesta vert-align text-center quad">
                            <div class="vert-cont">
                                <h2 class="tit-sesta no-icon"><span><span class="txt-light">Blog</span>{{flexi.titolo2}}</span></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-15 hidden-sm-down col-mosaic">
                        <img class="img-fluid" src="{{flexi.img_ret4.url}}" alt="{{flexi.img_ret4.alt}}" title="{{flexi.img_ret4.alt}}"/>
                        <img class="img-fluid quad" src="{{flexi.img_quad2.url}}" alt="{{flexi.img_quad2.alt}}" title="{{flexi.img_quad2.alt}}"/>
                    </div>
                    <div class="col-md-15 col-mosaic">
                        <div class="bg-dark-sesta vert-align text-center quad">
                            <div class="vert-cont">
                                <h2 class="tit-sesta no-icon"><span><span class="txt-light">Blog</span>{{flexi.titolo3}}</span></h2>
                            </div>
                        </div>
                        <img class="img-fluid" src="{{flexi.img_ret5.url}}" alt="{{flexi.img_ret2.alt}}" title="{{flexi.img_ret2.alt}}"/>
                    </div>
                </div>
            </div>
        </section>
    {% endif %}
{% endfor %}