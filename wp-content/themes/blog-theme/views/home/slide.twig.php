<header id="top">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            {% for post in slide_home %}
            {% if loop.first %}
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            {% else %}
            {% set counter = counter + 1%}
                <li data-target="#carouselExampleIndicators" data-slide-to="{{counter}}"></li>
            {% endif %}
            {% endfor %}
        </ol>
        <div class="carousel-inner" role="listbox">
            {% for post in slide_home %}
                <div class="carousel-item {% if loop.first %} active {% endif %}" style="background-image: url('{{ TimberImage(post.img_slide).src }}')"> </div>
            {% endfor %}
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <a class="to-down" href="#start" title=" "><img src="{{template_root}}/img/utility/top-grey.svg" alt=" " title=" " /></a>
    </div>
</header>